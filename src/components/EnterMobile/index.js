import React, { Component } from "react";
import { phoneNumberValidation, alert } from "../../utils";
import "./index.scss";
import { Link } from "react-router-dom";

export default class EnterMobile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errorDisplay: "none",
      mobile: ""
    };
  }
  _onChangeMobileInput = e => {
    const _mobile = e.currentTarget.value;
    this.setState({ mobile: phoneNumberValidation(_mobile) });
  };

  _onSubmited = async e => {
    e.preventDefault();
    this.props.onGetCodeClick(this.state.mobile);
  };

  render() {
    return (
      <div className="login">
        <div className="logo_campon">Kampon</div>
        <div className="login_container">
          <div className="login_head">
            <span>ورود</span>
          </div>
          <form onSubmit={this._onSubmited}>
            <div className="login_box-row">
              <span className="login_title">شماره موبایل:</span>
              <input
                maxlength="11"
                name="mobile"
                value={this.state.mobile}
                onChange={this._onChangeMobileInput}
                placeholder="09xxxxxxxxx"
                className="login_box-value"
              />
              <div
                className="alert_mobile"
                style={{ display: this.state.errorDisplay }}
              >
                {this.state.error}
              </div>
            </div>
            <div className="login_box">
              <div className="login_submit">
                <button>تایید</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
