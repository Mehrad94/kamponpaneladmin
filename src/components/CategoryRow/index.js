import React, { useState } from "react";
import "./index.scss";
const CategoryRow = ({
  category,
  onDelete,
  edit,
  block,
  number,
  valNumber,
  ShowSubCategory
}) => {
  console.log({ mojicat: category });
  // valNumber = (e, id) => {
  //   // console.log(e, id);
  //   valNumber = { e, id };
  // };

  return (
    <div id="Categorys">
      <div className="category_img">
        <img className="category_img-view" src={category.catImage} />
      </div>
      <div className="category_titles">
        <span className="cat_title">{category.catTitle}</span>
      </div>
      <div className="category_owner_details">
        <div>
          تعداد تخفیف ها :
          <span>{category.activeCount + category.deactiveCount}</span>
        </div>
        <div>
          تعداد فروشنده ها :<span>{category.owners}</span>
        </div>
        <div className="cat_number-true">
          <span>فعال</span>
          <span>{category.activeCount}</span>
        </div>
        <div className="cat_number-false">
          <span>غیر فعال</span>
          <span>{category.deactiveCount}</span>
        </div>
      </div>
      <div className="viewSubCategory" onClick={ShowSubCategory}>
        <span className="btnViewSubCategory">مشاهده زیر دسته ها</span>
      </div>
      {/* <div className="categoryWeight">
        <select value={category.weight} onChange={valNumber}>
          {number.map(num => (
            <option>{num}</option>
          ))}
        </select>
      </div> */}
      {/* <div className="category_number">
        
      </div> */}
      <div className="panel_options">
        <button className="delete" onClick={onDelete}>
          x
        </button>
        <button className="edit" onClick={edit} />
        <button className="block_owner" onClick={block}>
          --
        </button>
      </div>
    </div>
  );
};

export default CategoryRow;
