import React from "react";
import axios from "axios";
import { URL_ADMIN_REPORT } from "../values/strings";

const adminReport = async () => {
  const URL = URL_ADMIN_REPORT;
  try {
    const resAdminReport = axios.get(URL);
    return resAdminReport;
  } catch (error) {
    console.log(error);
    return false;
  }
};
export default adminReport;
