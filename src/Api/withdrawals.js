import axios from "axios";
import { URL_GET_WITHDRAWALS } from "../values/strings";

const withdrawals = async params => {
  console.log({ params });
  try {
    const reswithdrawals = await axios.get(URL_GET_WITHDRAWALS + "/" + params);
    return reswithdrawals.data;
  } catch (error) {
    console.log({ error });
    return [];
  }
};
export default withdrawals;
