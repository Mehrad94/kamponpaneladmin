import React from "react";
import { Route, Switch } from "react-router-dom";
import logo, { ReactComponent } from "./logo.svg";
import "./App.css";
import Navbar from "./components/Navbar";
import SideBar from "./components/SideBar";
import Dashboard from "./components/Dashboard";
import Owners from "./components/Owners";
import AddOwner from "./components/AddOwner/index";
import Login from "./components/Login";
import Category from "./components/Category/index";
import AddCategory from "./components/AddCategory";
import Discounts from "./components/Discounts";
import AddDiscount from "./components/AddDiscount";
import Sliders from "./components/Sliders";
import AddSlider from "./components/AddSlider";
import Members from "./components/Members";

import MemberRow from "./components/MemberRow";
import Transactions from "./components/Transactions";
import { TOKEN } from "./values/strings";
import District from "./components/District";
import AddDistrict from "./components/AddDistrict";
import Settings from "./components/Settings";
import Gift from "./components/Gift";
import AddGift from "./components/AddGift";
import Report from "./components/Report";
import SettingReport from "./components/SettingReport";
import OrderingCategory from "./components/OrderingCategory";
import AddSabCategory from "./components/AddSabCategory";

function App() {
  return (
    <div className="App">
      <div className="content">
        <Switch>
          <Route path="/" exact component={Login} />
          <Route path="/dashboard"
            render={() =>
              localStorage.getItem(TOKEN) ? (
                <React.Fragment>
                  <Navbar />
                  <SideBar />
                  <Dashboard />
                </React.Fragment>
              ) : ((window.location = "/"))
            }
          />
          <Route
            path="/category"
            render={() =>
              localStorage.getItem(TOKEN) ? (
                <React.Fragment>
                  <Navbar />
                  <SideBar />
                  <Category />
                </React.Fragment>
              ) : (
                  (window.location = "/")
                )
            }
          />
          <Route
            path="/OrderingCategory"
            render={() =>
              localStorage.getItem(TOKEN) ? (
                <React.Fragment>
                  <Navbar />
                  <SideBar />
                  <OrderingCategory />
                </React.Fragment>
              ) : (
                (window.location = "/")
              )
            }
          />

          <Route
            path="/AddSabCategory"
            render={() =>
              localStorage.getItem(TOKEN) ? (
                <React.Fragment>
                  <Navbar />
                  <SideBar />
                  <AddSabCategory />
                </React.Fragment>
              ) : (
                (window.location = "/")
              )
            }
          />
          <Route
            path="/owner"
            render={() =>
              localStorage.getItem(TOKEN) ? (
                <React.Fragment>
                  <Navbar />
                  <SideBar />
                  <Owners />
                </React.Fragment>
              ) : (
                  (window.location = "/")
                )
            }
          />
          <Route
            path="/addOwner"
            render={() =>
              localStorage.getItem(TOKEN) ? (
                <React.Fragment>
                  <Navbar />
                  <SideBar />
                  <AddOwner />
                </React.Fragment>
              ) : (
                  (window.location = "/")
                )
            }
          />
          <Route
            path="/AddCategory"
            render={() =>
              localStorage.getItem(TOKEN) ? (
                <React.Fragment>
                  <Navbar />
                  <SideBar />
                  <AddCategory />
                </React.Fragment>
              ) : (
                  (window.location = "/")
                )
            }
          />
          <Route
            path="/Discounts"
            render={() =>
              localStorage.getItem(TOKEN) ? (
                <React.Fragment>
                  <Navbar />
                  <SideBar />
                  <Discounts />
                </React.Fragment>
              ) : (
                  (window.location = "/")
                )
            }
          />

          <Route
            path="/AddDiscount"
            render={() =>
              localStorage.getItem(TOKEN) ? (
                <React.Fragment>
                  <Navbar />
                  <SideBar />
                  <AddDiscount />
                </React.Fragment>
              ) : (
                  (window.location = "/")
                )
            }
          />
          <Route
            path="/Sliders"
            render={() =>
              localStorage.getItem(TOKEN) ? (
                <React.Fragment>
                  <Navbar />
                  <SideBar />
                  <Sliders />
                </React.Fragment>
              ) : (
                  (window.location = "/")
                )
            }
          />
          <Route
            path="/AddSlider"
            render={() =>
              localStorage.getItem(TOKEN) ? (
                <React.Fragment>
                  <Navbar />
                  <SideBar />
                  <AddSlider />
                </React.Fragment>
              ) : (
                  (window.location = "/")
                )
            }
          />
          <Route
            path="/Members"
            render={() =>
              localStorage.getItem(TOKEN) ? (
                <React.Fragment>
                  <Navbar />
                  <SideBar />
                  <Members />
                </React.Fragment>
              ) : (
                  (window.location = "/")
                )
            }
          />
          <Route
            path="/MemberRow"
            render={() =>
              localStorage.getItem(TOKEN) ? (
                <React.Fragment>
                  <Navbar />
                  <SideBar />
                  <MemberRow />
                </React.Fragment>
              ) : (
                  (window.location = "/")
                )
            }
          />
          <Route
            path="/Transactions"
            render={() =>
              localStorage.getItem(TOKEN) ? (
                <React.Fragment>
                  <Navbar />
                  <SideBar />
                  <Transactions />
                </React.Fragment>
              ) : (
                  (window.location = "/")
                )
            }
          />
          <Route
            path="/District"
            render={() =>
              localStorage.getItem(TOKEN) ? (
                <React.Fragment>
                  <Navbar />
                  <SideBar />
                  <District />
                </React.Fragment>
              ) : (
                  (window.location = "/")
                )
            }
          />
          <Route
            path="/AddDistrict"
            render={() =>
              localStorage.getItem(TOKEN) ? (
                <React.Fragment>
                  <Navbar />
                  <SideBar />
                  <AddDistrict />
                </React.Fragment>
              ) : (
                  (window.location = "/")
                )
            }
          />
          <Route
            path="/Gift"
            render={() =>
              localStorage.getItem(TOKEN) ? (
                <React.Fragment>
                  <Navbar />
                  <SideBar />
                  <Gift />
                </React.Fragment>
              ) : (
                  (window.location = "/")
                )
            }
          />
          <Route
            path="/AddGift"
            render={() =>
              localStorage.getItem(TOKEN) ? (
                <React.Fragment>
                  <Navbar />
                  <SideBar />
                  <AddGift />
                </React.Fragment>
              ) : (
                  (window.location = "/")
                )
            }
          />
          <Route
            path="/Report"
            render={() =>
              localStorage.getItem(TOKEN) ? (
                <React.Fragment>
                  <Navbar />
                  <SideBar />
                  <Report />
                </React.Fragment>
              ) : (
                  (window.location = "/")
                )
            }
          />
          <Route
            path="/SettingReport"
            render={() =>
              localStorage.getItem(TOKEN) ? (
                <React.Fragment>
                  <Navbar />
                  <SideBar />
                  <SettingReport />
                </React.Fragment>
              ) : (
                  (window.location = "/")
                )
            }
          />
          <Route
            path="/Settings"
            render={() =>
              localStorage.getItem(TOKEN) ? (
                <React.Fragment>
                  <Navbar />
                  <SideBar />
                  <Settings />
                </React.Fragment>
              ) : (
                  (window.location = "/")
                )
            }
          />
        </Switch>
      </div>
    </div>
  );
}

export default App;
