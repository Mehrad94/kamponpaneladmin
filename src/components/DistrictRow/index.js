import React from "react";
import "./index.scss";
const DistrictRow = ({ District, onDelete }) => {
  console.log({ mojiDistrict: District });
  return (
    <div id="Districts">
      <div className="District_titles">
        <span className="District_title">{District.districtName}</span>
      </div>

      <button className="delete_District" onClick={onDelete}>
        x
      </button>
    </div>
  );
};

export default DistrictRow;
