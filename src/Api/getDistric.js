import axios from "axios";
import { URL_GETDISTRICT } from "./../values/strings/index";

const getDistrict = async () => {
  try {
    console.log("geting categorys");
    const resGetDistrict = await axios.get(URL_GETDISTRICT);
    console.log({ resGetDistrict });
    return resGetDistrict.data;
  } catch (error) {
    console.log({ error });
    return [];
  }
};
export default getDistrict;
