import axios from "axios";
import { URL_OWNER } from "../values/strings";

const getOwners = async () => {
  try {
    const resGetOwners = await axios.get(URL_OWNER);
    console.log({ resGetOwners });
    return resGetOwners.data;
  } catch (error) {
    console.log({ error });

    return [];
  }
};
export default getOwners;
