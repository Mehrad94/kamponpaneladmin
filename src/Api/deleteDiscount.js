import axios from "axios";
import { URL_GET_DISCOUNT } from "../values/strings";

const deleteDiscount = async discountId => {
  try {
    const resGetDiscount = await axios.delete(
      URL_GET_DISCOUNT + "/" + discountId
    );
    return resGetDiscount.data;
  } catch (error) {
    console.log({ error });

    return [];
  }
};
export default deleteDiscount;
