import React from "react";
const ReportRow = ({ reports }) => {
  console.log({ safhe2: reports });
  let member = reports.memberId;
  let discount = reports.disId;

  return (
    <div id="discounts" style={{}}>
      <div className="discount_img">
        <img className="discount_img-view" src={member.mAvatar} />
      </div>

      <div className="discount_titles">
        <div className="titles">
          <span className="cat_title">{member.mPhoneNumber}</span>
          <span className="cat_title">{member.mName}</span>
          <span className="cat_title">{member.mAddress}</span>
        </div>
        <div className="titles">
          <span className="cat_title" style={{ color: "green" }}>
            {discount.disOwner}
          </span>
          <span className="cat_title">{discount.disDescription}</span>
          <span className="cat_title" style={{ color: "brown" }}>
            {discount.disCategoryPersian}
          </span>
          <span className="cat_title">{reports.oTitle}</span>
          <span className="cat_title font12">{reports.oDescription}</span>
        </div>

        <div className="price_discount">
          <del style={{ color: "red" }} className="discount_RealPrice">
            {discount.disRealPrice}
          </del>
          <span style={{ color: "green" }} className="discount_NewPrice">
            {discount.disNewPrice}
            <span className="toman">تومان</span>
          </span>
        </div>
        <div className="titles">
          {reports.subjects.map(subject => (
            <span className="cat_title">{subject}</span>
          ))}
        </div>
        <div className="TimeAndTrackCode">
          <span>{reports.time}</span>
          <span>
            <span style={{ color: "blue" }}></span>:{reports.trackCode}
          </span>
        </div>
        <div className="panel_options">
          {/* <button className="delete" onClick={this.props.onDelete}>
        x
      </button>
      <button className="edit" onClick={this.props.edit} />
      <button className="block_owner" onClick={this.props.block}>
        --
      </button> */}
        </div>
      </div>
    </div>
  );
};
export default ReportRow;
