import React, { useState, useEffect } from "react";
import "./index.scss";
import { addCategory, getCategories } from "../../Api";
import { aSuccess, aFailed } from "../../utils";
const AddSabCategory = () => {
  const [categories, setCategories] = useState("");
  const [categoryId, setCategoryId] = useState("");
  const [categoryTitle, setCategoryTitle] = useState("");
  const [subCatTitle, setSubCatTitle] = useState("");
  const [subCatTitlePersian, setSubCatTitlePersian] = useState("");
  const [display, setDisplay] = useState("none");
  useEffect(() => {
    _handelGetCategory();
  }, []);
  const _handelGetCategory = async () => {
    const resCategories = await getCategories();
    if (resCategories) {
      setCategories(resCategories);
    }
  };
  const _handelChengeSubCatTitle = e => {
    let value = e.currentTarget.value;
    let name = e.currentTarget.name;
    setSubCatTitle(value);
    console.log({ value, name, subCatTitle });
  };
  const _handelChengeSubCatTitlePersian = e => {
    let value = e.currentTarget.value;
    let name = e.currentTarget.name;
    setSubCatTitlePersian(value);
    console.log({ value, name, subCatTitle });
  };
  const _handelCategoriesId = id => {
    //  let value=e.currentTarget.value;

    console.log({ id });
    // setCategoriesId({e})
  };
  const _handelSubmeted = async () => {
    let id = categoryId;
    let subCategory = {
      categoryId,
      subCatTitlePersian,
      subCatTitle
    };
    if (subCategory) {
      const resAddSubCategory = await addCategory("", "", "", subCategory);
      console.log({ resAddSubCategory });
      if (resAddSubCategory) {
        setCategoryId("");
        setCategoryTitle("");
        setSubCatTitle("");

        setSubCatTitlePersian("");

        return aSuccess("تایید", "زیر دسته با موفقیت ثبت شد");
      }
    } else {
      return aFailed("لطفا تمام تمام فیلد ها را پر کنید ");
    }
  };
  const _handelDisplay = () => {
    if (display === "none") {
      setDisplay("flex");
    } else {
      setDisplay("none");
    }
  };
  const _spanId = (id, catTitle) => {
    console.log({ id, catTitle });
    setCategoryId(id);
    setCategoryTitle(catTitle);
  };
  console.log({ categories, categoryId });

  return (
    <div className="AddSabCategory_container">
      <div className="AddSabCategory_box">
        <div className="boxHeadTitle">
          <span>تنظیم زیر دسته ها </span>
        </div>

        <div className="boxForInformation">
          <span className="boxForInformationTitle">
            دسته اصلی را انتخاب نمایید :
          </span>
          <div onClick={_handelDisplay} className="categoryTitle">
            {categoryTitle ? categoryTitle : "کلیک کنید"}
            <div className="categoriesContainer" style={{ display: display }}>
              <span>انتخاب کنید</span>
              {categories &&
                categories.map(category => (
                  <span
                    onClick={() => _spanId(category._id, category.catTitle)}
                  >
                    {category.catTitle}
                  </span>
                ))}
            </div>
          </div>
        </div>
        <div className="boxForInformation">
          <span className="boxForInformationTitle">
            نام زیر دسته جدید را انگلیسی وارد نمایید :
          </span>
          <input name="subCatTitle" onChange={_handelChengeSubCatTitle} />
        </div>
        <div className="boxForInformation">
          <span className="boxForInformationTitle">
            نام زیر دسته جدید را فارسی وارد نمایید :
          </span>
          <input
            name="subCatTitlePersian"
            onChange={_handelChengeSubCatTitlePersian}
          />
        </div>
        <div onClick={_handelSubmeted} className="btnSubmited">
          <span>ثبت اطلاعات</span>
        </div>
      </div>
    </div>
  );
};
export default AddSabCategory;
