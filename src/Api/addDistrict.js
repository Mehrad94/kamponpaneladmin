import axios from "axios";
import { URL_ADDDISTRICT } from "./../values/strings";

const addDistrict = async params => {
  console.log({ params });

  try {
    const resGetDistrict = await axios.post(URL_ADDDISTRICT, params);
    console.log({ resGetDistrict });
    return true;
  } catch (error) {
    console.log({ error });
    return false;
  }
};
export default addDistrict;
