import React, { Component } from "react";
import "./index.scss";
import axios from "axios";
import EnterMobile from "../EnterMobile/index";
import EnterToken from "../EnterToken";
import { BASE_URL, TOKEN } from "../../values/strings/index";

const Joi = require("@hapi/joi");
export default class Login extends Component {
  state = {
    error: "",
    aPhoneNumber: "",
    token: "",
    isToken: false,
    showToast: false
  };

  _onGetCodeClick = async aPhoneNumber => {
    try {
      const resSendMobileNumber = await axios.post(
        BASE_URL + "/admin/getCode",
        {
          aPhoneNumber
        }
      );
      console.log({ resSendMobileNumber });
      this.setState({ isToken: true, aPhoneNumber });
    } catch (e) {
      console.log("Shomare Vared shode Eshtebah ast");
    }
  };

  _onTokenEntered = async aCode => {
    console.log({ aCode });
    try {
      const resSendToken = await axios.post(BASE_URL + "/admin/verifyCode", {
        aPhoneNumber: this.state.aPhoneNumber,
        aCode
      });
      console.log({ resSendToken });
      localStorage.setItem(TOKEN, resSendToken.data.token);
      window.location = "/dashboard";
    } catch (error) {
      console.log({ error });
      this.setState({ showToast: true });
    }
  };

  render() {
    console.log({ SHOW: this.state.showToast, state: this.state });
    if (this.state.isToken)
      return (
        <EnterToken
          showToast={this.state.showToast}
          onTokenEntered={this._onTokenEntered}
        />
      );
    else
      return !localStorage.getItem(TOKEN) ? (
        <EnterMobile onGetCodeClick={this._onGetCodeClick} />
      ) : (
        (window.location = "/dashboard")
      );
  }
}

//09113352725

// _onSubmited = async e => {
//   e.preventDefault();
// };
// _onsend = async e => {
//   if (this.state.mobile.length == 11) {
//     this.setState({ token: "jabar" });

//     console.log({ state: this.state });
//     const params = { aPhoneNumber: this.state.mobile };
//     console.log("sending");

//     const natije = await axios.post(BASE_URL + "/admin/getCode", params);
//     console.log(natije);
//   }
// };
// _onMobileValueChange = value => {
//   this.setState({ mobile: value });
// };

// return this.state.token ? (
//   <div className="login">
//     <div className="logo_campon">Kampon</div>
//     <div className="login_container">
//       <div className="login_head">
//         <span>ورود</span>
//       </div>
//       <EnterToken />
//       <div className="login_box">
//         <form onSubmit={this._onSubmited}>
//           <div className="login_submit">
//             <Link to="/dashboard">
//               <button>تایید</button>
//             </Link>
//           </div>
//         </form>
//       </div>
//     </div>
//   </div>
// ) : (
//   <div className="login">
//     <div className="logo_campon">Kampon</div>
//     <div className="login_container">
//       <div className="login_head">
//         <span>ورود</span>
//       </div>
//       <EnterMobile onValueChange={this._onMobileValueChange} />

//       <div className="login_box">
//         <form onSubmit={this._onSubmited}>
//           <div className="login_submit">
//             <button onClick={this._onsend}>تایید</button>
//           </div>
//         </form>
//       </div>
//     </div>
//   </div>
// );
// this.state.ahmad ? (
//     <div className="login">
//       <div className="logo_campon">Kampon</div>
//       <div className="login_container">
//         <div className="login_head">
//           <span>ورود</span>
//         </div>
//         <EnterMobile />
//         <EnterToken />
//         <div className="login_box">
//           <form onSubmit={this._onSubmited}>
//             <div className="login_submit">
//               <button>تایید</button>
//             </div>
//           </form>
//         </div>
//       </div>
//     </div>
//   ) : (
//     <div className="login">
//       <div className="logo_campon">Kampon</div>
//       <div className="login_container">
//         <div className="login_head">
//           <span>ورود</span>
//         </div>
//         <EnterMobile />
//         <EnterToken />
//         <div className="login_box">
//           <form onSubmit={this._onSubmited}>
//             <div className="login_submit">
//               <button>تایید</button>
//             </div>
//           </form>
//         </div>
//       </div>
//     </div>
