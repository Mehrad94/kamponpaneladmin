import { TOKEN } from "../values/strings";
import axios from "axios";

export const setHeader = () => {
  axios.defaults.headers.common["authorization"] =
    "Bearer " + localStorage.getItem(TOKEN);
};
