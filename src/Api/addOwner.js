import axios from "axios";
import { URL_ADD_OWNER } from "../values/strings";

const addOwner = async (objParams, edit) => {
  console.log(objParams);
  console.log({ edit });

  try {
    if (edit == true) {
      const resEditOWner = await axios.put(
        URL_ADD_OWNER + "/" + objParams._id,
        objParams
      );

      console.log({ resEditOWner });
      return true;
    } else {
      const resAddOWner = await axios.post(URL_ADD_OWNER, objParams);
      console.log({ resAddOWner });
      return true;
    }
  } catch (error) {
    console.log({ error });
    return false;
  }
};

export default addOwner;
