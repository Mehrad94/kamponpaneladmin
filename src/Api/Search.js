import axios from "axios";
import { URL_SEARCH } from "../values/strings";
const Search = async searchString => {
  console.log({ searchString });
  try {
    const URL = URL_SEARCH;
    const rezSearch = await axios.post(URL, { searchString });
    console.log(rezSearch);
    return rezSearch.data;
  } catch (error) {
    console.log({ error });
    return false;
  }
};
export default Search;
