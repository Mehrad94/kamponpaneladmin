import axios from "axios";
import { URL_ADMIN_GIFT } from "./../values/strings";

const addGift = async (params, page) => {
  console.log({ params, page });
  let URL = URL_ADMIN_GIFT;
  if (!page) {
    try {
      const resAddGift = await axios.post(
        URL +
          "/" +
          params.discountId +
          "/" +
          params.memberId +
          "/" +
          params.count
      );
      console.log({ resAddGift });
      return true;
    } catch (error) {
      console.log({ error });
      return false;
    }
  } else {
    try {
      const resGetGift = await axios.get(URL + "/" + page);
      console.log({ resGetGift });
      return resGetGift.data;
    } catch (error) {
      console.log({ error });
      return false;
    }
  }
};
export default addGift;
