import React, { Component } from "react";
import "./index.scss";
import {
  addDiscount,
  uploadImage,
  searchOwners,
  getCategories,
  getOwners,
  getDiscount,
  addCategory
} from "../../Api";

import { aSuccess, aFailed } from "../../utils";
import { ok } from "assert";
import { async } from "q";

const defaultAvatar =
  "https://cdn2.iconfinder.com/data/icons/instagram-ui/48/jee-75-512.png";
export default class AddDiscount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      discount: {
        disTitle: "",
        disDescription: "",
        disImages: [],
        disRealPrice: "",
        disNewPrice: "",
        disCategory: "",
        disOwner: "",
        disFeatures: [],
        disTermsOfUse: [],
        ownerId: "",
        disThumbnail: null
      },
      disfeacher: "",
      disterms: "",
      categories: [],
      imageFile: null,
      isImageUpload: false,
      discountImage: defaultAvatar,
      searchedOwners: [],
      isSearch: false,
      isOwnerName: "",
      modal: "",
      removing: "",
      valueRemove: "",
      nameRemove: "",
      edit: false,
      loading: true,
      subCategory: [],
      showSubcategory: "none",
      display: "none",
      subCategoryTitle: ""
    };
  }
  locationdiscount = window.location.href;

  discountLocationId = this.locationdiscount.split("/");

  locationLength = this.discountLocationId.length;

  async componentDidMount() {
    const categories = await getCategories();
    if (categories) {
      this.setState({
        categories
      });
    }

    const txt = [
      "وجه کمپُن خریداری شده قابل استرداد نیست",
      "کسب و کار ملزم به رعایت کلیه موارد درج شده در این پیشنهاد است",
      "قبل از تهیه کمپُن بررسی های لازم را به عمل آورید"
    ];
    let discount = this.state.discount;
    if (categories.length > 0) {
      discount.disCategory = this.state.categories[0].catTitle;
    }

    this.setState({
      discount
    });

    let discountId = this.discountLocationId[
      this.discountLocationId.length - 1
    ];
    console.log({ discountId });

    if (this.locationLength == 5) {
      let alldiscounts = await getDiscount("", "", discountId);
      // alldiscounts = alldiscounts.docs;
      console.log({ alldiscounts });
      if (alldiscounts) {
        let discountOwner = alldiscounts;
        // if (alldiscounts) {
        //   discountOwner = alldiscounts.filter(discounts => {
        //     return discounts._id === discountId;
        //   });
        // }
        let disCategory = discountOwner.disCategory;
        let catId = discountOwner.catId;
        console.log({ disCategory });
        if (disCategory === "goods") {
          console.log({ catId });
          this._handelGetSubCategory(catId, disCategory);
        }
        let disOwner = discountOwner;
        let ownerDiscount = discountOwner;

        console.log({ disOwner });

        this.setState({
          discount: ownerDiscount,
          edit: true
        });

        const oName = this.state.discount.ownerId.oName;
        const ownerId = this.state.discount.ownerId._id;
        let discount = this.state.discount;

        console.log({ ownerId });

        discount.disOwner = oName;
        discount.ownerId = ownerId;

        this.setState({ discount });
        console.log({ jabar: disOwner });
        console.log({ discount });
        console.log({ alldiscounts });
      } else {
        return aFailed("خطا ");
      }
    }
    if (this.locationLength !== 5) {
      const discount = this.state.discount;
      let disFeatures = discount.disFeatures;
      disFeatures = txt;
      // let disFeatere = "";
      // txt.map(txt => (disFeatere += txt.toString));
      console.log({ disFeatures });
    }
  }

  _handleSubmit = async e => {
    e.preventDefault();
    const { discount, edit } = this.state;
    if (this.locationLength != 5) {
      const txt1 = "وجه کمپُن خریداری شده قابل استرداد نیست";

      const txt2 =
        "کسب و کار ملزم به رعایت کلیه موارد درج شده در این پیشنهاد است";
      const txt3 = "قبل از تهیه کمپُن بررسی های لازم را به عمل آورید";
      let disTerms = discount.disTermsOfUse;

      if (!disTerms.includes(txt1 || txt2 || txt3)) {
        discount.disTermsOfUse.push(txt1, txt2, txt3);
        console.log("true");
      }

      console.log({ discount4: discount });

      this.setState({ discount });
    }

    if (
      !Object.values(discount).every(o => {
        if (typeof o === "number") {
          return o.toString().length > 0;
        } else if (typeof o === "string") {
          return o.length > 0;
        } else {
          return true;
        }
      })
    ) {
      return aFailed("اطلاعات ناقص", "لطفا فرم را کامل  نمایید");
    }

    if (await addDiscount(discount, edit)) {
      this.setState({
        discount: {
          ...discount,

          disTitle: "",
          disDescription: "",
          disImages: [],
          disRealPrice: "",
          disNewPrice: "",
          disOwner: "",
          disThumbnail: ""
        },
        imageFile: null
      });

      return aSuccess("تایید", "تخفیف با موفقیت ثبت شد");
    } else
      return aFailed(
        "خطا در ثبت اطلاعات ",
        "متاسفانه اطلاعات ثبت نشد دوباره تلاش کنید"
      );
  };

  _handleChange = async e => {
    let value = e.currentTarget.value;
    let newState = this.state;
    newState.discount[e.currentTarget.name] = value;
    this.setState({ state: newState });

    if (e.currentTarget.name === "disOwner" && value.length > 2) {
      let resSearchOwners = await searchOwners(value);
      console.log({ resSearchOwners });

      this.setState({ searchedOwners: resSearchOwners, isSearch: true });
      // newState[e.disOwner].push(searchOwners);
    }
  };

  _handelImageUpload = async e => {
    console.log({ e });

    let file = e.currentTarget.files[0];
    console.log({ file });

    await this.setState({ imageFile: file });
    let imgUrl = await uploadImage(this.state.imageFile);
    let discount = this.state.discount;
    discount.disImages.push(imgUrl);
    this.setState({ discount });
  };
  _handelImagedisThumbnail = async e => {
    let discount = this.state.discount;
    console.log({ e });
    let file = e.currentTarget.files[0];
    console.log({ file });
    await this.setState({ imageFile: file });
    let disThumbnail = await uploadImage(this.state.imageFile);
    discount.disThumbnail = disThumbnail;
    this.setState({ discount });
    console.log({ state: this.state });
  };

  _handeldiscount = e => {
    let value = e.currentTarget.value;

    let newState = this.state;
    newState[e.currentTarget.name] = value;

    this.setState({ state: newState });
    // console.log({ val: newState });
  };

  _handelDiscountFeateres = e => {
    let Feateres = this.state.disfeacher;
    console.log({ shal: Feateres });
    let terms = this.state;
    terms[e.currentTarget.name] = Feateres;
    let disFeatures = this.state.discount.disFeatures;
    disFeatures.push(Feateres);
    const moji = this.state.discount.disFeatures;
    // console.log({kk:moji})
    this.setState({ moji: disFeatures, disfeacher: "" });
    console.log({ disFeatures });
  };

  _handelDiscountdisTermsOfUse = e => {
    let disterms = this.state.disterms;
    console.log({ shal: disterms });
    let terms = this.state;
    terms[e.currentTarget.name] = disterms;
    let disTermsOfUse = this.state.discount.disTermsOfUse;
    disTermsOfUse.push(disterms);
    const moji = this.state.discount.disTermsOfUse;
    // console.log({kk:moji})
    this.setState({ moji: disTermsOfUse, disterms: "" });
    console.log({ disTermsOfUse });
  };

  _onDeleteTitles = (value, name) => {
    console.log("okeye");
    let newDiscount = this.state.discount;
    newDiscount[name] = this.state.discount[name].filter(dis => {
      return dis !== value;
    });
    this.setState({ discount: newDiscount });
  };
  _question = (value, name) => {
    this.setState({ nameRemove: name, valueRemove: value });
    this.setState({ modal: "actived " });
  };

  _questionFalse = () => {
    this.setState({ modal: "" });
  };
  _questiontrue = () => {
    let state = this.state;
    let value = state.valueRemove;
    let name = state.nameRemove;

    this._onDeleteTitles(value, name);
    this.setState({ removing: "", modal: "" });
  };

  _onSearchItemClick = async (ownerId, ownerName) => {
    console.log({ ownerName });
    let discount = this.state.discount;
    discount.disOwner = ownerName;
    discount.ownerId = ownerId;

    // console.log({ disOwners });
    this.setState({ isSearch: false, discount });

    // console.log(searchedOwners);
    let dis = this.state.discount;
    let disOwner = dis.disOwner;
    let searchedOwners = this.state.searchedOwners;
    let owner = searchedOwners.filter(owners => {
      return owners.oName === disOwner;
    });
    console.log({ owner, disOwner });
    let oCategory = owner[0].oCategory;
    let catId = owner[0].catId;
    console.log({ catId, oCategory });

    this._handelGetSubCategory(catId, oCategory);

    console.log({ state: this.state });
  };
  _handelGetSubCategory = async (catId1, oCategory, disCategory) => {
    console.log("suuuuuuuuuub", disCategory, oCategory, catId1);
    let showSubcategory = "none";
    if (oCategory === "goods") {
      showSubcategory = "block";
    } else {
      showSubcategory = "none";
    }
    this.setState({
      showSubcategory
    });
    let catId = "";
    if (catId1) {
      catId = catId1;
    } else {
      catId = disCategory;
    }
    const resGetSubCategory = await getCategories(catId);

    if (resGetSubCategory) {
      this.setState({
        subCategory: resGetSubCategory.data
      });
      let disSubcategory = this.state.discount.subCategory;
      let stateSubCategory = this.state.subCategory;
      if (stateSubCategory) {
        let subCategoryTitle = stateSubCategory.filter(subCategory => {
          return subCategory._id === disSubcategory;
        });
        console.log({ disSubcategory, stateSubCategory, subCategoryTitle });
        if (subCategoryTitle.length > 0) {
          let subCategoryPersianTitle = subCategoryTitle[0].subCatTitlePersian;

          this.setState({ subCategoryTitle: subCategoryPersianTitle });
        }
        console.log({ resGetSubCategory });
      }
    }
  };
  _handelSubCategory = e => {
    let value = e.currentTarget.value;
    let name = e.currentTarget.name;
    console.log({ value });
    // let subCategoryMoji = this.state.discount.subCategory;
    let newDiscount = this.state.discount;
    newDiscount[name] = value;
    console.log({ value, name });
    // this.setState({ discount: newDiscount });
    console.log({ state: this.state });
  };
  _handelDisplay = () => {
    let display = "";
    if (this.state.display === "none") {
      display = "flex";
    } else {
      display = "none";
    }
    this.setState({ display });
  };
  _spanId = (id, catTitle) => {
    console.log({ id, catTitle });
    this.setState({ subCategoryTitle: catTitle });
    let newDiscount = this.state.discount;
    newDiscount["subCategory"] = id;

    // setCategoryId(id);
    // setCategoryTitle(catTitle);
  };
  render() {
    const locationLength = this.locationLength;
    console.log({ state: this.state });
    const {
      display,
      subCategory,
      showSubcategory,
      discount,
      subCategoryTitle
    } = this.state;

    return (
      <div className="adddiscount_container">
        <div id="modal_question" className={this.state.modal}>
          <span>آیا مطمئن به پاک کردن این مورد هستید؟</span>
          <div className="remove_accept">
            <span className="green" onClick={this._questiontrue}>
              بله
            </span>
            <span className="orange" onClick={this._questionFalse}>
              خیر
            </span>
          </div>
        </div>
        <div className="discount">
          <div className="adddiscount_row">
            <div className="adddiscount_header-box">
              <span>
                {locationLength == 5
                  ? "مشخصات تخفیفتان را تغییر دهید"
                  : "مشخصات تخفیف جدید"}
              </span>
              <label className="label_images">
                <input
                  id="fileupload"
                  name="fileupload"
                  className="category_img-enter "
                  type="file"
                  onChange={this._handelImageUpload}
                />
                <span className="add_images">افزودن عکس</span>
              </label>
            </div>
            <form onSubmit={this._handleSubmit}>
              <div className="D_avatar-insert">
                <img
                  for="fileupload"
                  className="preview"
                  src={this.state.discount.disThumbnail}
                />
                <label className="img_browser">
                  <span className="add_img-owner">+</span>
                  <input
                    id="fileupload"
                    name="disThumbnail"
                    className="owner_img-enter "
                    type="file"
                    onChange={this._handelImagedisThumbnail}
                  />
                </label>
              </div>
              <div className="discount_avatar-insert">
                {this.state.discount.disImages.map(image => (
                  <img
                    name="disImages"
                    className="preview_discount"
                    src={image}
                    onClick={() => this._question(image, "disImages")}
                  />
                ))}
              </div>
              <div className="discount_row positionRealetive">
                <span className="discount_row-title"> نام فروشنده: </span>
                <input
                  autoComplete="off"
                  placeholder="نام فروشنده را وارد کنید"
                  className="discount_row-value "
                  name="disOwner"
                  type="text"
                  value={this.state.discount.disOwner}
                  onChange={this._handleChange}
                />
                <div
                  className="disOwner_search"
                  style={
                    this.state.isSearch ? { display: "" } : { display: "none" }
                  }
                >
                  <div className="disOwner_search-title">
                    {this.state.searchedOwners.map(owner => (
                      <div
                        className="sellers"
                        onClick={() =>
                          this._onSearchItemClick(owner._id, owner.oName)
                        }
                      >
                        <div className="sellerImg">
                          <img src={owner.oAvatar} />
                        </div>
                        <div className="sellerNameCategory">
                          <span className="sellerName">{owner.oName}</span>
                          <span className="sellerCategory">
                            {owner.oCategory}
                          </span>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
              <div className="discount_row">
                <span className="discount_row-title">عنوان تخفیف:</span>
                <input
                  placeholder=" عنوان تخفیف را وارد کنید"
                  className="discount_row-value"
                  name="disTitle"
                  type="text"
                  value={this.state.discount.disTitle}
                  onChange={this._handleChange}
                />
              </div>
              {/* <div
                style={{ display: showSubcategory }}
                className="discount_row"
              >
                <span className="discount_row-title">زیر دسته:</span> */}

              {/* <select
                  value={discount.subCategory}
                  name="subCategory"
                  onChange={this._handelSubCategory}
                >
                  <option>انتخاب کنید</option>
                  {subCategory &&
                    subCategory.map(subCategory => (
                      <option>{subCategory}</option>
                    ))}
                </select> */}
              <div
                style={{ display: showSubcategory }}
                className="discount_row boxForInformation"
              >
                <span className="boxForInformationTitle">
                  یک زیر دسته انتخاب نمایید :
                </span>
                <div
                  style={{ marginTop: "16px" }}
                  onClick={this._handelDisplay}
                  className="categoryTitle"
                >
                  {discount.subCategory ? subCategoryTitle : "کلیک کنید"}
                  <div
                    className="categoriesContainer"
                    style={{ display: display }}
                  >
                    <span>انتخاب کنید</span>
                    {subCategory &&
                      subCategory.map(subCategory => (
                        <span
                          onClick={() =>
                            this._spanId(
                              subCategory._id,
                              subCategory.subCatTitlePersian
                            )
                          }
                        >
                          {subCategory.subCatTitlePersian}
                        </span>
                      ))}
                  </div>
                </div>
                {/* </div> */}
              </div>
              <div className="discount_row">
                <span className="discount_row-title"> توضیحات تخفیف :</span>
                <input
                  placeholder="توضیحات تخفیف را وارد کنید"
                  className="discount_row-value"
                  name="disDescription"
                  type="text"
                  value={this.state.discount.disDescription}
                  onChange={this._handleChange}
                />
              </div>
              <div className="discount_prices">
                <div className="discount_row">
                  <span className="discount_row-title"> قیمت اصلی :</span>
                  <input
                    placeholder="قیمت اصلی را وارد کنید"
                    className="discount_row-value"
                    name="disRealPrice"
                    type="text"
                    value={this.state.discount.disRealPrice}
                    onChange={this._handleChange}
                  />
                </div>
                <div className="discount_row">
                  <span className="discount_row-title"> قیمت جدید :</span>
                  <input
                    placeholder="قیمت جدید را وارد کنید"
                    className="discount_row-value"
                    name="disNewPrice"
                    type="text"
                    value={this.state.discount.disNewPrice}
                    onChange={this._handleChange}
                  />
                </div>
              </div>

              <div className="discount_row  positionRealetive">
                <span
                  className="disTermsOFuse_send"
                  name="disfeacher"
                  onClick={this._handelDiscountFeateres}
                >
                  ثبت
                </span>
                <div className="discount_row-title-search">
                  {this.state.discount.disFeatures.map(disFeatere => (
                    <span
                      onClick={() => this._question(disFeatere, "disFeatures")}
                    >
                      {disFeatere}
                    </span>
                  ))}
                </div>
                <span className="discount_row-title"> ویژگی های تخفیف :</span>
                <input
                  placeholder="ویژگی های تخفیف را وارد کنید"
                  className="discount_row-value"
                  name="disfeacher"
                  type="text"
                  value={this.state.disfeacher}
                  onChange={this._handeldiscount}
                />
              </div>

              <div className="discount_row positionRealetive">
                <span
                  name="disterms"
                  className="disTermsOFuse_send"
                  onClick={this._handelDiscountdisTermsOfUse}
                >
                  ثبت
                </span>
                <div className="discount_row-title-search">
                  {this.state.discount.disTermsOfUse.map(disTermsOf => (
                    <span
                      onClick={() =>
                        this._question(disTermsOf, "disTermsOfUse")
                      }
                    >
                      {disTermsOf}
                    </span>
                  ))}
                </div>
                <span className="discount_row-title"> شرایط استفاده :</span>
                <input
                  placeholder="شرایط استفاده را وارد کنید"
                  className="discount_row-value"
                  name="disterms"
                  type="text"
                  value={this.state.disterms}
                  onChange={this._handeldiscount}
                />
              </div>

              {/* <div className="discount_row">
                <span className="discount_row-title">دسته تخفیف:</span>
                <select
                  className="discount_row-value"
                  name="disCategory"
                  value={this.state.discount.disCategory}
                  onChange={this._handleChange}
                >
                  {this.state.categories.map(cat => (
                    <option>{cat.catTitle}</option>
                  ))}
                </select>
              </div> */}

              <input
                className="btn_discount-submit"
                type="submit"
                value="ثبت "
              />
            </form>
          </div>
        </div>
      </div>
    );
  }
}
