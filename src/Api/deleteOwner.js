import axios from "axios";
import { URL_OWNER } from "../values/strings";

const deleteOwner = async ownerId => {
  const deleteOwner = await axios.delete(URL_OWNER + "/" + ownerId);
  console.log({ deleteOwner });
};
export default deleteOwner;
