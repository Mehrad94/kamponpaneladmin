import React, { useState } from "react";
import { Link } from "react-router-dom";
import "./index.scss";
import icDashboard from "../../assets/images/ic_dashboard.png";
import icCategory from "../../assets/images/ic_category.png";
import icSeller from "../../assets/images/ic_seller.png";
import icOwner from "../../assets/images/ic_seller.png";
import icSlide from "../../assets/images/ic_slide.png";
import icDiscount from "../../assets/images/ic_discount.png";
import icDistrict from "../../assets/images/ic_district.png";
import icMember from "../../assets/images/ic_member.png";
import icWithdraw from "../../assets/images/ic_member.png";
import icGift from "../../assets/images/ic_gift.png";
import icSetting from "../../assets/images/ic_setting.png";
import icReport from "../../assets/images/ic_report.png";
const SideBar = () => {

  const [selectedMenu, setSelectedMenu] = useState("/dashboard");
  const [menus, setMenu] = useState([
    {
      to: "/dashboard",
      title: "داشبورد",
      icon: icDashboard
    }, {
      to: "/category",
      title: "دسته بندی ها",
      icon: icCategory
    }, {
      to: "/owner",
      title: "فروشندها",
      icon: icSeller
    }, {
      to: "/Sliders",
      title: "اسلایدها",
      icon: icSlide
    }, {
      to: "/discounts",
      title: "تخفیف ها",
      icon: icDiscount
    }, {
      to: "/District",
      title: "محدوده ها",
      icon: icDistrict
    }, {
      to: "/Members",
      title: "اعضای سایت",
      icon: icMember
    }, {
      to: "/Transactions",
      title: "درخواست های برداشت",
      icon: icWithdraw
    }, {
      to: "/Gift",
      title: "جوایز",
      icon: icGift
    }, {
      to: "/Report",
      title: "گزارش",
      icon: icReport
    }, {
      to: "/Settings",
      title: "تنظیمات",
      icon: icSetting
    }
  ]);

  const onMenuClicked = (to) => {
    setSelectedMenu(to);
  }

  return (
    <div className="sideBar">
      <div className="sidebar_container">
        <ul className="sidebar_ul">
          {menus.map((menu) => (
            <li onClick={() => onMenuClicked(menu.to)} >
              <Link to={menu.to}>
                <div>
                  <div className={selectedMenu === menu.to ? "menuHolderActive" : "menuHolder"}>
                    <img className="menuIcon" src={menu.icon} alt="Dashboard" />
                    <span className="menuTitle">{menu.title}</span>
                  </div>
                  <div className="hr" />
                </div>
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}

export default SideBar;