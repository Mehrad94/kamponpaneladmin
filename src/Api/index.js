import searchOwners from "./searchOwners";
import deleteCategory from "./deleteCategory";
import addDiscount from "./addDiscount";
import getDiscount from "./getDiscount";
import uploadImage from "./uploadImage";
import addOwner from "./addOwner";
import getCategories from "./getCategories";
import getSlider from "./getSlider";
import addSlider from "./addSlider";
import getOwners from "./getOwners";
import { setHeader } from "../utils/setHeader";
import addCategory from "./addCategory";
import deleteOwner from "./deleteOwner";
import deleteDiscount from "./deleteDiscount";
import getMember from "./getMember";
import withdrawals from "./withdrawals";
import cashOut from "./cashOut";
import getDistrict from "./getDistric";
import addDistrict from "./addDistrict";
import deleteDistrict from "./deleteDistrict";
import addLinks from "./addLinks";
import getLinks from "./getLinks";
import addSellerApp from "./addSellerApp";
import deleteSlider from "./deleteSlider";
import Search from "./Search";
import MemberSearch from "./MemberSearch";
import addGift from "./addGift";
import adminReport from "./adminReport";
import reportSubjects from "./reportSubjects";
import categoryWeight from "./categoryWeight";
import addVersionsApps from "./addVersionsApps";

setHeader();
export {
  addCategory,
  addDiscount,
  addOwner,
  deleteCategory,
  getSlider,
  getCategories,
  getOwners,
  deleteOwner,
  searchOwners,
  uploadImage,
  getDiscount,
  deleteDiscount,
  getMember,
  withdrawals,
  cashOut,
  addSlider,
  getDistrict,
  addDistrict,
  deleteDistrict,
  addLinks,
  getLinks,
  addSellerApp,
  deleteSlider,
  Search,
  MemberSearch,
  addGift,
  adminReport,
  reportSubjects,
  categoryWeight,
  addVersionsApps
};
