import React, { Component } from "react";
import "./index.scss";
import { aSuccess, aFailed } from "../../utils";
import Swal from "sweetalert2";
import "sweetalert2/src/sweetalert2.scss";
import {
  uploadImage,
  addSlider,
  getDiscount,
  getCategories,
  getSlider,
  Search
} from "../../Api";
import { stat } from "fs";
import { TOKEN } from "../../values/strings";

const defaultAvatar =
  "https://cdn2.iconfinder.com/data/icons/instagram-ui/48/jee-75-512.png";
export default class AddSlider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageFile: null,
      isImageUpload: false,
      discounts: [],
      sImage: defaultAvatar,
      types: [],
      sType: "",
      sDiscountId: "",
      _id: "",
      searchDiscountsString: "",
      searchDiscounts: "",
      isSearchDiscount: false,
      disTitle: "",
      display: "none",
      subCategorys: [],
      showSubcategory: "none",
      display: "none",
      subCategoryTitle: "",
      subCategory: ""
    };
  }
  locationslider = window.location.href;

  sliderLocationId = this.locationslider.split("/");

  locationLength = this.sliderLocationId.length;
  async componentDidMount() {
    //get discount from server
    // const discounts = await getDiscount();
    // if (discounts.lenght > 0) {
    //   discounts.unshift({ disTitle: "لطفا تخفیف را انتخاب کنید" });
    // }

    //get categories from server
    let types = await getCategories();

    // //add main attribute to the first place of the categories array
    if (types.length) {
      // this.setState({ sType: this.state.types[0].catTitle });
      this.setState({ types });
    }

    // //log discounts and categories
    // console.log({ discounts, types });

    // // set state the categories

    // initialize sType

    let sliderId = this.sliderLocationId[this.sliderLocationId.length - 2];
    let sliderSType = this.sliderLocationId[this.sliderLocationId.length - 1];
    console.log({ sliderId, sliderSType });
    if (this.locationLength >= 5) {
      let allSliders = await getSlider(sliderSType);
      const slider = allSliders.filter(sliders => {
        return sliders._id.includes(sliderSType);
      });
      this.setState(slider[0]);
      this.setState({ isImageUpload: true, edit: true });
      console.log({ slider });
      console.log({ allSliders });
    }
    console.log(this.sliderLocationId);
  }

  _handelSubmit = async e => {
    e.preventDefault();
    const {
      sType,
      sDiscountId,
      sImage,
      imageFile,
      isImageUpload,
      subCategory
    } = this.state;
    if (sDiscountId === "" || !isImageUpload || sType == "") {
      console.log({ MOPOJIIIIIIII: this.state });
      return aFailed("خطا", "لطفا اطلاعات را کامل وارد نمایید");
    }
    console.log({ state: this.state });

    const params = { sType, sDiscountId, sImage, subCategory };
    console.log({ params });
    if (await addSlider(params)) {
      this.setState({ sImage: defaultAvatar });
      aSuccess("با موفقیت", "ثبت شد");
    } else aFailed("خطا", "ثبت نشد");
  };

  // _handleChange = e => {
  //   let value = e.currentTarget.value;
  //   const selectedDiscount = this.state.discounts.filter(discount => {
  //     return discount.disTitle.includes(value);
  //   });
  //   console.log({ value, selectedDiscount });

  //   this.setState({ sDiscountId: selectedDiscount[0]._id });
  // };

  _handelImageUpload = async e => {
    let file = e.currentTarget.files[0];
    await this.setState({ imageFile: file });

    let imgUrl = await uploadImage(this.state.imageFile);
    console.log({ imgUrl });
    this.setState({ sImage: imgUrl, isImageUpload: true });
  };
  _giveDiscountId = sDiscountId => {
    console.log({ sDiscountId });
    this.setState({ sDiscountId });
  };

  _onClickCategories = cat => {
    console.log({ cat });
    if (cat === "main") {
      this.setState({ sType: cat, showSubcategory: "none" });
    } else {
      let types = this.state.types;
      let sTypes = types.filter(type => {
        return type.catPersianTitle === cat;
      });
      let catTitle = sTypes[0].catTitle;
      let catId = sTypes[0]._id;

      this._handelGetSubCategory(catId, catTitle);
      console.log({ sTypes, catTitle, catId });
      this.setState({ sType: catTitle });
    }
  };
  // _handelSearchDiscount = async e => {
  //   let value = e.currentTarget.value;
  //   console.log({ value });
  //   if (value.lenght > 2) {
  //     const searchDiscountsString = await Search();
  //     if (searchDiscountsString) {
  //       this.setState({ searchDiscountsString });
  //     }
  //   }
  // };
  _handleChange = async e => {
    let value = e.currentTarget.value;
    let name = e.currentTarget.name;
    let newState = this.state;
    console.log(value);
    newState[name] = value;
    this.setState(newState);
    /////////discountName/////////
    if (name === "disTitle" && value.length > 2) {
      let resSearchDiscount = await Search(value);
      console.log(resSearchDiscount);

      this.setState({
        searchDiscounts: resSearchDiscount.discounts,
        isSearchDiscount: true
      });
      // newState[e.disSearchDiscount].push(searchDiscount);
    } else if (name === "disTitle" && value.length <= 2) {
      this.setState({
        searchDiscounts: "",
        isSearchDiscount: false
      });
    }
  };
  _onSearchItemClick = (name, nameId, nameTitle) => {
    console.log({ nameTitle, nameId });
    let discountClicked = this.state.searchDiscounts.filter(dis => {
      return dis.disTitle === nameTitle;
    });
    console.log({ discountClicked });
    if (name === "discounts") {
      this.setState({
        isSearchDiscount: false,
        disTitle: nameTitle,
        sDiscountId: nameId
      });
    }
    if (discountClicked) {
      // let catId = discountClicked[0].catId;
      // let oCategory = discountClicked[0].disCategory;
      // this._handelGetSubCategory(catId, oCategory);
      // console.log({ catId, oCategory });
    }

    console.log({ state: this.state });
  };
  _handelDisplay = () => {
    let display = "";
    if (this.state.display === "none") {
      display = "flex";
    } else {
      display = "none";
    }
    this.setState({ display });
  };
  _spanId = (id, catTitle) => {
    console.log({ id, catTitle });
    this.setState({ subCategoryTitle: catTitle, sType: id });
    let state = this.state;
    state["subCategory"] = id;

    // setCategoryId(id);
    // setCategoryTitle(catTitle);
  };
  _handelGetSubCategory = async (catId1, oCategory, disCategory) => {
    console.log("suuuuuuuuuub", disCategory, oCategory, catId1);
    let showSubcategory = "block";
    // if (oCategory === "goods") {
    //   showSubcategory = "block";
    // } else {
    //   showSubcategory = "none";
    // }
    this.setState({
      showSubcategory
    });
    let catId = "";
    if (catId1) {
      catId = catId1;
    } else {
      catId = disCategory;
    }
    const resGetSubCategory = await getCategories(catId);

    if (resGetSubCategory) {
      this.setState({
        subCategorys: resGetSubCategory.data
      });
      let disSubcategory = this.state.subCategory;
      let stateSubCategory = this.state.subCategory;
      if (stateSubCategory) {
        let subCategoryTitle = stateSubCategory.filter(subCategory => {
          return subCategory._id === disSubcategory;
        });
        console.log({ disSubcategory, stateSubCategory, subCategoryTitle });
        if (subCategoryTitle.length > 0) {
          let subCategoryPersianTitle = subCategoryTitle[0].subCatTitlePersian;

          this.setState({ subCategoryTitle: subCategoryPersianTitle });
        }
        console.log({ resGetSubCategory });
      }
    }
  };
  render() {
    const locationLength = this.locationLength;
    console.log({ state: this.state });
    const {
      discounts,
      display,
      disTitle,
      isSearchDiscount,
      searchDiscounts,
      showSubcategory,
      subCategoryTitle,
      subCategorys
    } = this.state;

    return (
      <div className="addSlider_container">
        <div className="addSlider_row">
          <div className="addSlider_header-box">مشخصات تخفیف جدید</div>
          <form onSubmit={this._handelSubmit}>
            <div className="Slider_avatar-insert">
              <img
                for="fileupload"
                className="preview"
                src={this.state.sImage}
              />
              <label className="img_browser_slider">
                <span className="add_img-Slider">+</span>
                <input
                  id="fileupload"
                  name="fileupload"
                  className="Slider_img-enter "
                  type="file"
                  onChange={this._handelImageUpload}
                />
              </label>
            </div>
            <div className="enter_Slider-name-slider">
              <span>نام تخفیف جدید را وارد کنید</span>
              <div className="BoxSearch">
                <input
                  autoComplete="off"
                  value={disTitle}
                  onChange={this._handleChange}
                  name="disTitle"
                  type="text"
                  placeholder="تخفیف مورد نظرتان را تایپ کنید"
                />
                <div
                  className="discount_search searchSlider"
                  style={
                    isSearchDiscount ? { display: "" } : { display: "none" }
                  }
                >
                  <div className="discount_search-title">
                    {searchDiscounts &&
                      searchDiscounts.length &&
                      searchDiscounts.map(discount => (
                        <div
                          className="discounts"
                          onClick={() =>
                            this._onSearchItemClick(
                              "discounts",
                              discount._id,
                              discount.disTitle
                            )
                          }
                        >
                          <div className="discountImg">
                            <img src={discount.disImages[0]} />
                          </div>
                          <div className="discountNameCategory">
                            <span className="discountName">
                              {discount.disTitle}
                            </span>
                            <span className="discountCategory">
                              {discount.disCategory}
                            </span>
                          </div>
                        </div>
                      ))}
                  </div>
                </div>
              </div>

              {/* <select name="sDiscountId" onChange={this._handleChange}>
                {discounts.lenght > 0 &&
                  discounts.map(discounts => (
                    <option>{discounts.disTitle}</option>
                  ))}
              </select> */}
              <span>نام دسته را وارد کنید</span>
              <select
                onChange={e => this._onClickCategories(e.currentTarget.value)}
              >
                <option>main</option>
                {this.state.types.map(category => (
                  <option>{category.catPersianTitle}</option>
                ))}
              </select>

              <div
                style={{ display: showSubcategory }}
                className="discount_row boxForInformation"
              >
                <span className="boxForInformationTitle">
                  یک زیر دسته انتخاب نمایید :
                </span>
                <div
                  style={{ marginTop: "16px" }}
                  onClick={this._handelDisplay}
                  className="categoryTitle"
                >
                  {subCategorys ? subCategoryTitle : "کلیک کنید"}
                  <div
                    className="categoriesContainer"
                    style={{ display: display }}
                  >
                    <span>انتخاب کنید</span>
                    {subCategorys &&
                      subCategorys.map(subCategory => (
                        <span
                          onClick={() =>
                            this._spanId(
                              subCategory._id,
                              subCategory.subCatTitlePersian
                            )
                          }
                        >
                          {subCategory.subCatTitlePersian}
                        </span>
                      ))}
                  </div>
                </div>
                {/* </div> */}
              </div>
            </div>

            <input className="btn_Slider-submit" type="submit" value="ثبت " />
          </form>
        </div>
      </div>
    );
  }
}
