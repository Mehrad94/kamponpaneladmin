import React from "react";
import "./index.scss";
const sliderRow = ({ slider, onDelete, edit, block }) => {
  console.log({ mojicat: slider });
  return (
    <div id="sliders">
      <div className="slider_img">
        <img className="slider_img-view" src={slider.sImage} />
      </div>
      <div className="slider_titles">
        <span className="cat_title">{slider.sType}</span>
        <span className="cat_title">{slider.sDiscountId}</span>
      </div>
      <div className="slider_number">
        <div className="cat_number-true">
          <span>فعال</span>
          <span>10</span>
        </div>
        <div className="cat_number-false">
          <span>غیر فعال</span>
          <span>30</span>
        </div>
      </div>
      <div className="panel_options">
        <button className="delete" onClick={onDelete}>
          x
        </button>
        <button className="edit" onClick={edit} />
        <button className="block_owner" onClick={block}>
          --
        </button>
      </div>
    </div>
  );
};

export default sliderRow;
