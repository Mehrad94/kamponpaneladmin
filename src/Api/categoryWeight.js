import React, { Component } from "react";
import { CATEGORY_WEIGHT } from "../values/strings/index";
import Axios from "axios";
const categoryWeight = async params => {
  let URL = CATEGORY_WEIGHT;

  try {
    const resPatch = await Axios.patch(URL, params);
    console.log({ resPatch });
    return true;
  } catch (error) {
    console.log({ error });
    return true;
  }
};
export default categoryWeight;
