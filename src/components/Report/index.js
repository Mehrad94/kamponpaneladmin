import React, { useEffect, useState } from "react";
import { adminReport } from "../../Api/index";
import { Link } from "react-router-dom";
import "./index.scss";
import ReportRow from "../ReportRow";
const Report = () => {
  const [reports, setReports] = useState(null);
  const [maps, setMaps] = useState(false);
  useEffect(() => {
    adminReportGet();
  }, []);
  const adminReportGet = async () => {
    if (await adminReport()) {
      const resReports = await adminReport();
      let Reports = resReports.data;
      console.log(Reports);
      setReports(Reports);
      setMaps(true);
    }
  };
  console.log({ reports });
  let key = 0;
  return (
    <div className="ReportContainer">
      <div className="Report_head">
        <div className="Report_title"> گزارش ها</div>
        <Link to="/SettingReport">
          <button className="Report_submit">تنظیمات</button>
        </Link>
      </div>
      <div className="Report_list">
        {maps === true &&
          reports.map(report => <ReportRow key={key++} reports={report} />)}
      </div>
      {/* <div className="pages">
          {numberPage.map(page => (
            <span
              onClick={() => this._handelchengePage(page)}
              className={this.state.page === page ? "page actived" : "page"}
            >
              {page}
            </span>
          ))}
        </div> */}
    </div>
  );
};
export default Report;
