import React, { Component } from "react";
import { Link } from "react-router-dom";
import { addGift } from "../../Api";
import GiftRow from "../GiftRow";
import "./index.scss";
class Gift extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gifts: [],
      maps: false,
      page: 1,
      numberPage: []
    };
  }
  async componentDidMount() {
    this._handelGiveGifts();
  }
  _handelGiveGifts = async pages => {
    const { gifts, maps, page } = this.state;
    let Npage = "";
    if (!pages) {
      Npage = page;
    } else {
      Npage = pages;
    }
    const rezGifts = await addGift("", Npage);
    console.log({ rezGifts });
    if (rezGifts.docs.length>0) {
      this.setState({ gifts: rezGifts, maps: true });
      console.log({ jafar: rezGifts });
      const numberPage = [];
      for (let i = 1; i <= rezGifts.pages; i++) {
        console.log({ i });
        numberPage.push(i);
      }
      console.log({ numberPage });
      this.setState({ numberPage });
    }
  };
  _handelchengePage = page => {
    this.setState({ page });
    this._handelGiveGifts(page);
  };
  render() {
    const { gifts, maps, page, numberPage } = this.state;

    let key = 0;
    console.log({ state: this.state });
    console.log({ giftsDocs: gifts.docs });
    return (
      <div className="GiftContainer">
        <div className="Gift_head">
          <div className="Gift_title"> جوایز ها</div>
          <Link to="/addGift">
            <button className="Gift_submit">جایزه دادن</button>
          </Link>
        </div>
        <div className="Gift_list">
          {maps === true &&
            gifts.docs.map(gift => <GiftRow key={key++} gift={gift} />)}
        </div>
        <div className="pages">
          {numberPage.map(page => (
            <span
              onClick={() => this._handelchengePage(page)}
              className={this.state.page === page ? "page actived" : "page"}
            >
              {page}
            </span>
          ))}
        </div>
      </div>
    );
  }
}
export default Gift;
