import React, { Component } from "react";
import "./index.scss";
import { getLinks, addLinks, uploadImage, addVersionsApps } from "../../Api";
import { async } from "q";
import { aSuccess, aFailed } from "../../utils";
class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      links: {
        memberApplication: "",
        sellerApplication: "",
        facebook: "",
        instagram: "",
        telegram: "",
        twitter: "",
        backToAppLink: ""
      },
      versions: {
        memberApplication: "",
        sellerApplication: ""
      },
      memberApplication: "",
      sellerApplication: ""
    };
  }

  _handelChengeLinks = e => {
    let links = this.state.links;
    links[e.currentTarget.name] = e.currentTarget.value;
    console.log({ links });

    this.setState({ links });
  };
  _handelChengeVersions = e => {
    let versions = this.state.versions;
    versions[e.currentTarget.name] = e.currentTarget.value;
    console.log({ versions });

    this.setState({ versions });
  };

  _sendLink = async e => {
    // const resSendLinks = await addLinks({
    //   sellerApplication: this.state.links.sellerApplication
    // });
    let name = e.currentTarget.name;
    console.log({ name });
    let param = {};
    param[name] = this.state.links[name];

    if (await addLinks(param)) {
      return aSuccess("تایید", "لینک با موفقیت ثبت شد");
    } else
      return aFailed(
        "خطا در ثبت اطلاعات ",
        "متاسفانه اطلاعات ثبت نشد دوباره تلاش کنید"
      );
  };
  _sendVersion = async e => {
    // const resSendLinks = await addLinks({
    //   sellerApplication: this.state.links.sellerApplication
    // });
    let name = e.currentTarget.name;
    console.log({ name });
    let param = {};
    param[name] = this.state.versions[name];
    console.log({ param });
    const resChengeAppVersion = await addVersionsApps(param);
    console.log({ resChengeAppVersion });
    if (resChengeAppVersion) {
      return aSuccess("تایید", "لینک با موفقیت ثبت شد");
    } else
      return aFailed(
        "خطا در ثبت اطلاعات ",
        "متاسفانه اطلاعات ثبت نشد دوباره تلاش کنید"
      );
  };

  // _sendMemberApplicationLink = async () => {
  //   console.log({ MEMEMEMEMMEE: this.state.links.memberApplication });

  //   // const resSendLinks = await addLinks({
  //   //   memberApplication: this.state.links.memberApplication
  //   // });
  //   if (
  //     await addLinks({
  //       memberApplication: this.state.links.memberApplication
  //     })
  //   ) {
  //     return aSuccess("تایید", "لینک اپ مشتری با موفقیت ثبت شد");
  //   } else
  //     return aFailed(
  //       "خطا در ثبت اطلاعات ",
  //       "متاسفانه اطلاعات ثبت نشد دوباره تلاش کنید"
  //     );
  // };
  _handelAppUpload = async e => {
    let file = e.currentTarget.files[0];
    console.log({ file });
    const name = e.currentTarget.name;
    console.log({ name });

    let state = this.state;
    let newLinks = state.links;
    console.log({ newLinks1: newLinks });
    state[name] = file;
    console.log({ state });

    await this.setState(state);
    let appUrl = await uploadImage(this.state[name]);
    newLinks[name] = appUrl;
    console.log({ appUrl });
    console.log({ newLinks });

    this.setState({ links: newLinks });
    // let links = this.state.links;
    // links.disapps.push(appUrl);
    // this.setState({ links });
  };
  render() {
    console.log({ state: this.state });
    return (
      <div className="settings">
        <div className="settings_container">
          <div className="setting_inpute-form">
            <div className="download_app_sellers">
              <span>لینک دانلود اپ فروشنده:</span>
              <div className="input_seller">
                <button
                  name="sellerApplication"
                  className="submited_app-link"
                  onClick={this._sendLink}
                >
                  ثبت
                </button>
                <label className="add_new_upload">
                  <span> آپلود</span>
                  <input
                    name="sellerApplication"
                    className="upload_app"
                    type="file"
                    onChange={this._handelAppUpload}
                  />
                </label>

                <input
                  disabled
                  className="upload_app_txt"
                  name="sellerApplication"
                  value={this.state.links.sellerApplication}
                />
              </div>
            </div>

            <div className="download_app_customers">
              <span>لینک دانلود اپ مشتری:</span>

              <div className="input_customer">
                <input
                  disabled
                  className="upload_app_txt"
                  name="memberApplication"
                  value={this.state.links.memberApplication}
                  onChange={this._handelChengeLinks}
                />
                <button
                  name="memberApplication"
                  className="submited_app-link"
                  onClick={this._sendLink}
                >
                  ثبت
                </button>
                <label className="add_new_upload">
                  <span> آپلود</span>
                  <input
                    className="upload_app"
                    name="memberApplication"
                    type="file"
                    onChange={this._handelAppUpload}
                  />
                </label>
              </div>
            </div>
            <div className="link_box">
              <span>لینک اینستاگرام:</span>

              <div className="input_link">
                <input
                  className="upload_app_txt"
                  name="instagram"
                  value={this.state.links.instagram}
                  onChange={this._handelChengeLinks}
                />
                <button
                  name="instagram"
                  className="submited_app-link"
                  onClick={this._sendLink}
                >
                  ثبت
                </button>
              </div>
            </div>
            <div className="link_box">
              <span>لینک تلگرام:</span>

              <div className="input_link">
                <input
                  className="upload_app_txt"
                  name="telegram"
                  value={this.state.links.telegram}
                  onChange={this._handelChengeLinks}
                />
                <button
                  name="telegram"
                  className="submited_app-link"
                  onClick={this._sendLink}
                >
                  ثبت
                </button>
              </div>
            </div>
            <div className="link_box">
              <span>لینک توئیتر:</span>

              <div className="input_link">
                <input
                  className="upload_app_txt"
                  name="twitter"
                  value={this.state.links.twitter}
                  onChange={this._handelChengeLinks}
                />
                <button
                  name="twitter"
                  className="submited_app-link"
                  onClick={this._sendLink}
                >
                  ثبت
                </button>
              </div>
            </div>

            <div className="link_box">
              <span>لینک بازگشت به اپ:</span>
              <div className="input_link">
                <input
                  className="upload_app_txt"
                  name="backToAppLink"
                  value={this.state.links.backToAppLink}
                  onChange={this._handelChengeLinks}
                />
                <button
                  name="backToAppLink"
                  className="submited_app-link"
                  onClick={this._sendLink}
                >
                  ثبت
                </button>
              </div>
            </div>
            <div className="link_box">
              <span>ورژن نرم افزار فروشنده:</span>
              <div className="input_link">
                <input
                  className="upload_app_txt"
                  name="sellerApplication"
                  value={this.state.versions.sellerApplication}
                  onChange={this._handelChengeVersions}
                />
                <button
                  name="sellerApplication"
                  className="submited_app-link"
                  onClick={this._sendVersion}
                >
                  ثبت
                </button>
              </div>
            </div>
            <div className="link_box">
              <span>ورژن نرم افزار مشتری:</span>
              <div className="input_link">
                <input
                  className="upload_app_txt"
                  name="memberApplication"
                  value={this.state.versions.memberApplication}
                  onChange={this._handelChengeVersions}
                />
                <button
                  name="memberApplication"
                  className="submited_app-link"
                  onClick={this._sendVersion}
                >
                  ثبت
                </button>
              </div>
            </div>
            <div className="link_box">
              <span>آخرین ورژن نرم افزار:</span>
              <div className="input_link">
                <input
                  className="upload_app_txt"
                  name="backToAppLink"
                  value={""}
                  onChange={this._handelChengeLinks}
                />
                <button
                  name="backToAppLink"
                  className="submited_app-link"
                  onClick={this._sendLink}
                >
                  ثبت
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  async componentDidMount() {
    const resGetlink = await getLinks();
    console.log({ resGetlink });
    if (resGetlink == null) return "";
    this.setState({
      links: resGetlink.sLinks,
      versions: resGetlink.sLastVersions
    });
  }
}

export default Settings;
