import React, { Component } from "react";
import { Link } from "react-router-dom";
import { getCategories, deleteCategory, categoryWeight } from "../../Api";
import OrderingCategoryRow from "../OrderingCategoryRow/index";
import { TOKEN } from "../../values/strings";
import { aSuccess, aFailed } from "../../utils";
export default class OrderingCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: [],
      modal: "",
      removing: "",
      number: [],
      catWeight: "",
      params: {},
      checkNumbers: []
    };
  }
  async componentDidMount() {
    console.log("okl0-okl0-okl");
    const categories = await getCategories();
    console.log({ categories });
    if (categories) {
      let categoryLength = categories.length;
      let number = [];
      for (let i = 1; i <= categoryLength; i++) {
        number.push(i);
      }
      console.log(categoryLength);
      this.setState({ categories, number });
    }
  }

  _handelDelete = async catId => {
    await deleteCategory(catId);
    this.componentDidMount();
  };
  _handelEdite = categoryId => {
    console.log({ abas: categoryId });
    window.location = "/addcategory/" + categoryId;
  };
  _question = value => {
    this.setState({ valueRemove: value });
    this.setState({ modal: "actived " });
  };
  _questionFalse = () => {
    this.setState({ modal: "" });
  };
  _questiontrue = () => {
    let state = this.state;
    let value = state.valueRemove;

    this._handelDelete(value);
    this.setState({ removing: "", modal: "" });
  };
  _handelChengLevel = async (weight, id) => {
    let params = this.state.params;
    let checkNumbers = this.state.checkNumbers;
    checkNumbers.push(weight);

    params[id] = Number(weight);
    this.setState({ params, checkNumbers });
    let categories = this.state.categories;
    let cat = categories.filter(category => {
      return category._id === id;
    });

    cat[0].weight = weight;
    console.log({ cat });
    // console.log({ newState });
    // const { params } = this.state;
    // let val = "";
    // let param = { weight: Number(weight), id };
    // console.log({ param });
    // // const catWeight = await addCategory(params, "catWeight");
    // // console.log({ catWeight });
    // val = params;
    // // let params =  param;
    // console.log({ val });
    // this.setState({ params: val });
    // // this.componentDidMount();
  };
  _handelSendWeight = async () => {
    let params = this.state.params;
    // let checkNumbers = this.state.checkNumbers;
    // checkNumbers

    // let param = params.split(",");
    console.log({ params });
    const resPatchWeights = await categoryWeight(params);
    if (resPatchWeights) {
      console.log(resPatchWeights);
      aSuccess("تایید", "با موفقیت انجام شد");
    }
  };
  render() {
    console.log({ state: this.state });
    const { number } = this.state;
    return (
      <div className="category">
        <div className="category_container">
          <div className="head_category">
            <div className="category_title"> ترتیب بندی دسته بندی ها</div>

            <button
              onClick={this._handelSendWeight}
              className="category_submit"
            >
              ثبت
            </button>
          </div>
          {/* <div className="optionsWithPage">
            <Link to="/Ordering">
              <div className="optionsWithPageBox">
                <span className="optionsPageTitle">ترتیب دسته بندی</span>
              </div>
            </Link>
          </div> */}
          <div id="modal_question" className={this.state.modal}>
            <span>آیا مطمئن به پاک کردن این مورد هستید؟</span>
            <div className="remove_accept">
              <span className="green" onClick={this._questiontrue}>
                بله
              </span>
              <span className="orange" onClick={this._questionFalse}>
                خیر
              </span>
            </div>
          </div>
          <div className="category_list">
            {this.state.categories.map(category => (
              <OrderingCategoryRow
                edit={() => this._handelEdite(category._id)}
                category={category}
                onDelete={() => {
                  this._question(category._id);
                }}
                number={number}
                valNumber={e =>
                  this._handelChengLevel(e.currentTarget.value, category._id)
                }
              />
            ))}
          </div>
        </div>
      </div>
    );
  }
}
