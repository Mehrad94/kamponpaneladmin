import React, { Component } from "react";
import "./index.scss";
import { Link } from "react-router-dom";
import { getCategories, deleteCategory, addCategory } from "../../Api";
import CategoryRow from "../CategoryRow/index";
import { TOKEN } from "../../values/strings";
import { aSuccess, aFailed } from "../../utils";
export default class Category extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: [],
      modal: "",
      removing: "",
      number: [],
      catWeight: "",
      allSubCategory: [],
      display: "none",
      onClickCategoryId: ""
    };
  }
  async componentDidMount() {
    console.log("okl0-okl0-okl");
    const categories = await getCategories();
    console.log({ categories });
    let categoryLength = categories.length;
    let number = [];
    for (let i = 1; i <= categoryLength; i++) {
      number.push(i);
    }
    console.log(categoryLength);
    this.setState({ categories, number });
  }

  _handelDelete = async catId => {
    await deleteCategory(catId);
    this.componentDidMount();
  };
  _handelEdite = categoryId => {
    console.log({ abas: categoryId });
    window.location = "/addcategory/" + categoryId;
  };
  _question = value => {
    this.setState({ valueRemove: value });
    this.setState({ modal: "actived " });
  };
  _questionFalse = () => {
    this.setState({ modal: "" });
  };
  _questiontrue = () => {
    let state = this.state;
    let value = state.valueRemove;

    this._handelDelete(value);
    this.setState({ removing: "", modal: "" });
  };
  _handelChengLevel = async (weight, id) => {
    console.log({ weight, id });
    let params = { weight: Number(weight), id };
    const catWeight = await addCategory(params, "catWeight");
    console.log({ catWeight });
    this.setState({ catWeight });
    this.componentDidMount();
  };
  _handelViewSubCategory = async catId => {
    console.log("javadRide");
    const SubCategories = await getCategories(catId);
    console.log({ SubCategories });
    if (SubCategories.data.length) {
      this.setState({
        allSubCategory: SubCategories.data,
        display: "flex",
        onClickCategoryId: catId
      });
    } else {
      aFailed("زیر دسته موجود نمیباشد");
    }
  };
  _handelSubCategoryRemove = async SubCategory => {
    console.log({ SubCategory });

    let catId = this.state.onClickCategoryId;
    const { _id, subCatTitlePersian, subCatTitle } = SubCategory;
    let subCatId = _id;
    let params = { subCatTitle, subCatTitlePersian, subCatId };
    const DeleteSubCategories = await getCategories(catId, params);
    if (DeleteSubCategories) {
      console.log({ DeleteSubCategories });
      this._handelViewSubCategory(catId);
      aSuccess("زیر دسته با موفقیت حذف شد");
    } else {
      aFailed("زیر دسته با حذف نشد");
    }
  };
  _handelDisplaySubCaegory = () => {
    this.setState({ display: "none" });
  };

  render() {
    console.log({ state: this.state });
    const { number, allSubCategory, display } = this.state;
    return (
      <div className="category">
        <div className="category_container">
          <div className="head_category">
            <div className="category_title"> دسته بندی ها</div>
            <Link to="/addcategory">
              <button className="category_submit">افزودن دسته جدید</button>
            </Link>
            <Link to="/addSabCategory">
              <button className="category_submit">افزودن زیردسته جدید</button>
            </Link>
          </div>
          <div className="optionsWithPage">
            <Link to="/OrderingCategory">
              <div className="optionsWithPageBox">
                <span className="optionsPageTitle">ترتیب دسته بندی</span>
              </div>
            </Link>
          </div>
          <div id="modal_question" className={this.state.modal}>
            <span>آیا مطمئن به پاک کردن این مورد هستید؟</span>
            <div className="remove_accept">
              <span className="green" onClick={this._questiontrue}>
                بله
              </span>
              <span className="orange" onClick={this._questionFalse}>
                خیر
              </span>
            </div>
          </div>
          <div className="category_list">
            {this.state.categories.map(category => (
              <CategoryRow
                edit={() => this._handelEdite(category._id)}
                category={category}
                onDelete={() => {
                  this._question(category._id);
                }}
                ShowSubCategory={() =>
                  this._handelViewSubCategory(category._id)
                }
                number={number}
                valNumber={e =>
                  this._handelChengLevel(e.currentTarget.value, category._id)
                }
              />
            ))}
          </div>
          <div style={{ display: display }} className="modalShowSubContainer">
            <div className="categoryHeadTitle"></div>
            {allSubCategory &&
              allSubCategory.map(SubCategory => (
                <span>
                  {SubCategory.subCatTitlePersian}
                  <i
                    onClick={() => this._handelSubCategoryRemove(SubCategory)}
                  />
                </span>
              ))}
            <div className="close" onClick={this._handelDisplaySubCaegory}>
              x
            </div>
          </div>
        </div>
      </div>
    );
  }
}
