import React, { Component } from "react";
import "./index.scss";
class GiftRow extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { gift } = this.props;
    console.log({ giftssss: gift });
    return (
      <div id="discounts" style={{}}>
        <div className="discount_img">
          <img className="discount_img-view" src={gift.oImages[0]} />
          <span className="percent"> %{gift.oPercent}</span>
        </div>

        <div className="discount_titles">
          <div className="titles">
            <span className="cat_title">{gift.oOwner.oName}</span>
            <span style={{ color: "green" }} className="cat_title">
              {gift.oMember.mName}
            </span>
            <span style={{ color: "green" }} className="cat_title">
              {gift.oMember.mPhoneNumber}
            </span>
            <span className="cat_title">{gift.oTitle}</span>
            <span className="cat_title font12">{gift.oDescription}</span>
          </div>

          <div className="price_discount">
            <del style={{ color: "red" }} className="discount_RealPrice">
              {gift.oRealPrice}
            </del>
            <span style={{ color: "green" }} className="discount_NewPrice">
              {gift.oNewPrice}
              <span className="toman">تومان</span>
            </span>
          </div>
          <div className="TimeAndTrackCode">
            <span>{gift.time}</span>
            <span>
              <span style={{ color: "blue" }}>کد کمپن</span>:{gift.trackCode}
            </span>
          </div>
          <div className="panel_options">
            {/* <button className="delete" onClick={this.props.onDelete}>
              x
            </button>
            <button className="edit" onClick={this.props.edit} />
            <button className="block_owner" onClick={this.props.block}>
              --
            </button> */}
          </div>
        </div>
      </div>
    );
  }
}

export default GiftRow;
