import React, { Component } from "react";
import "./index.scss";
import { tokenValidation } from "../../utils/phoneNumberValidation";
import { ToastContainer, toast } from "react-toastify";
import "../../../node_modules/react-toastify/dist/ReactToastify.css";

export default class EnterToken extends Component {
  constructor(props) {
    super(props);
    this.state = { token: null };
  }
  _limitToken = e => {
    const _token = e.currentTarget.value;
    this.setState({ token: tokenValidation(_token) });
  };

  _onSubmited = e => {
    e.preventDefault();
    this.props.onTokenEntered(this.state.token);
  };

  componentWillReceiveProps(nextProps, prevProps) {
    if (nextProps.showToast) toast("توکن اشتباه هست");
  }

  render() {
    return (
      <div className="login">
        <div className="logo_campon">Kampon</div>
        <div className="login_container">
          <div className="login_head">
            <span>تایید شماره همراه</span>
          </div>
          <form onSubmit={this._onSubmited}>
            <div className="token">
              <span className="token_title">کد چهار را وارد نمایید</span>
              <input
                onChange={this._limitToken}
                className="token_box-value"
                maxlength="4"
                value={this.state.token}
                name="token"
                placeholder="****"
              />
            </div>
            <div className="login_box">
              <div className="login_submit">
                <button onClick={this._onsend}>تایید</button>
              </div>
            </div>
          </form>
        </div>
        <ToastContainer style={{}} />
      </div>
    );
  }
}
