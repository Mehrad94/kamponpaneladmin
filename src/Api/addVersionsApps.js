import React from "react";
import { ADMIN_SETTINGS } from "../values/strings";
import Axios from "axios";
const addVersionsApps = async param => {
  let url = ADMIN_SETTINGS;
  console.log({ param });
  try {
    const resChengVersionApp = await Axios.post(url, param);
    console.log(resChengVersionApp);
    if (resChengVersionApp) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    console.log({ error });
    return false;
  }
};
export default addVersionsApps;
