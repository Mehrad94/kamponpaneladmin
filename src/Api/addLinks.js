import axios from "axios";
import { URL_ADD_APP_LINK, URL_ADD_LINK } from "../values/strings";

const addLinks = async params => {
  console.log({ params });

  try {
    const resAddLinks = await axios.post(URL_ADD_LINK, params);
    console.log({ url: resAddLinks });
    return true;
  } catch (error) {
    console.log({ error });
    return false;
  }
};
export default addLinks;
