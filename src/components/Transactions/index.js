import React, { Component } from "react";
import "./index.scss";
import { Link } from "react-router-dom";
import TransactionRow from "../TransactionRow";
import { withdrawals, cashOut } from "../../Api";

export default class Transactions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Transactions: [],
      isWaiting: true,
      tabs: "tabOne"
    };
  }
  async componentDidMount() {
    await this._handelWithdrawal("Waiting", true, "tabOne");
  }

  _handelWithdrawal = async (into, isWaiting, tabs) => {
    this.setState({ tabs });
    const withdrawal = await withdrawals(into);
    this.setState({ Transactions: withdrawal, isWaiting });
  };

  _reply = async (answer, requestId) => {
    const cashed = await cashOut({ answer, requestId });
    this.componentDidMount();
    console.log({ cashed });
  };
  render() {
    console.log({ state: this.state });
    let key = 0;
    return (
      <div className="Transaction">
        <div className="Transaction_container">
          <div className="head_Transaction">
            <div className="Transactions_title"> درخواست های برداشت</div>
          </div>
          <div className="Transaction_tabs">
            <div
              className={this.state.tabs === "tabOne" ? "activingTabs" : "tab"}
              onClick={() => this._handelWithdrawal("Waiting", true, "tabOne")}
            >
              <span className="withdrawal"> در انتظار تایید </span>
            </div>
            <div
              className={this.state.tabs === "tabTwo" ? "activingTabs" : "tab"}
              onClick={() =>
                this._handelWithdrawal("Accepted", false, "tabTwo")
              }
            >
              <span className="withdrawal">تایید شده ها</span>
            </div>
            <div
              className={
                this.state.tabs === "tabthree" ? "activingTabs" : "tab"
              }
              onClick={() =>
                this._handelWithdrawal("Rejected", false, "tabthree")
              }
            >
              <span className="withdrawal">رد شده ها</span>
            </div>
          </div>
          <div className="Transaction_list">
            {this.state.Transactions.map(Transaction => (
              <TransactionRow
                key={key++}
                Transaction={Transaction}
                isWaiting={this.state.isWaiting}
                reply={this._reply}
                onDelete={() => {
                  this._handelDelete(Transaction._id);
                }}
              />
            ))}
          </div>
        </div>
      </div>
    );
  }
}
