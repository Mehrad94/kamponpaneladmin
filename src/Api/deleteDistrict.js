import axios from "axios";
import { URL_ADDDISTRICT } from "../values/strings";

const deleteDistrict = async DistrictId => {
  console.log({ DistrictId });

  try {
    const resDeleteDistrict = await axios.delete(
      URL_ADDDISTRICT + "/" + DistrictId
    );
    console.log({ resDeleteDistrict });
    return true;
  } catch (error) {
    console.log({ error });
    return false;
  }
};
export default deleteDistrict;
