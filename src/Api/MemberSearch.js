import axios from "axios";
import { URL_MEMBER_SEARCH } from "../values/strings";
const MemberSearch = async searchString => {
  console.log({ searchString });
  try {
    const URL = URL_MEMBER_SEARCH;
    const rezMemberSearch = await axios.post(URL, { searchString });
    console.log(rezMemberSearch);
    return rezMemberSearch.data;
  } catch (error) {
    console.log({ error });
    return false;
  }
};
export default MemberSearch;
