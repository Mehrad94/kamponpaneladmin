import axios from "axios";
import { URL_CATEGORY } from "./../values/strings/index";

const getCategories = async (catId, params) => {
  console.log({ catId, params });

  try {
    if (catId && !params) {
      const resGetCatId = await axios.get(URL_CATEGORY + "/" + catId);
      console.log({ resGetCatId });
      return resGetCatId;
    } else if (catId && params) {
      console.log({ params });
      const resDeleteSubCatId = await axios.delete(
        URL_CATEGORY + "/subCategory/" + catId,
        { params }
      );
      console.log({ resDeleteSubCatId });
      return resDeleteSubCatId;
    } else {
      console.log("geting categorys");
      const resGetCategories = await axios.get(URL_CATEGORY);
      console.log({ resGetCategories });
      return resGetCategories.data;
    }
  } catch (error) {
    console.log({ error });
    return [];
  }
};
export default getCategories;
