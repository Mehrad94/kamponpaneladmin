import React, { Component } from "react";
import "./index.scss";
import { TOKEN } from "../../values/strings";
export default class MemberRow extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    console.log({ moj: this.props });
    return (
      <div
        id="Members"
        style={{
          boxShadow: !this.props.member.mIsActive && this.props.boxShadow
        }}
      >
        <div className="Member_img">
          <img className="Member_img-view" src={this.props.member.mAvatar} />
        </div>

        <div className="Member_titles">
          <span className="cat_title memberName">
            {this.props.member.mName}
          </span>
          <span>{this.props.member.mAddress}</span>
          <span className="cat_title font12">
            {this.props.member.mPhoneNumber}
          </span>
          <div className="Member_details">
            <span>
              <span className="row"> ایمیل : </span>

              {this.props.member.mEmail}
            </span>
          </div>
        </div>
        <div>
          <span className="row">تعداد خرید :</span>
          <span className="">{this.props.member.mCart}</span>
        </div>

        <div className="panel_options">
          <button className="delete" onClick={this.props.onDelete}>
            x
          </button>
          <button className="edit" onClick={this.props.edit} />
          <button className="block_owner" onClick={this.props.memberBlock}>
            --
          </button>
        </div>
      </div>
    );
  }
}
// disTitle: "",
// disDescription: "",
// disImages: [],
// disRealPrice: "",
// disNewPrice: "",
// disCategory: " ",
// disOwner: "",
// disFeateres: [],
// disTermsOfUse: []
