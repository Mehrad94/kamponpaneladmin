import React, { Component } from "react";
import "./index.scss";
import { Link } from "react-router-dom";
import { getDistrict, deleteDistrict } from "../../Api";
import DistrictRow from "../DistrictRow";

export default class District extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Districts: []
    };
  }
  async componentDidMount() {
    console.log("okl0-okl0-okl");
    const Districts = await getDistrict();
    console.log({ Districts });
    if (Districts) {
      this.setState({ Districts });
    }
  }

  _handelDelete = async DistrictId => {
    await deleteDistrict(DistrictId);
    this.componentDidMount();
  };
  render() {
    return (
      <div className="District">
        <div className="District_container">
          <div className="head_District">
            <div className="District_title"> محدوده ها</div>
            <Link to="/AddDistrict">
              <button className="District_submit">+ثبت</button>
            </Link>
          </div>
          <div className="District_list">
            {this.state.Districts.map(District => (
              <DistrictRow
                District={District}
                onDelete={() => {
                  this._handelDelete(District._id);
                }}
              />
            ))}
          </div>
        </div>
      </div>
    );
  }
}
