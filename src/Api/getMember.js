import axios from "axios";
import { URL_GET_MEMBER } from "../values/strings";

const getMember = async (param, edit) => {
  try {
    if (edit == true) {
      console.log("yyyyy");

      const resEditMember = await axios.put(
        URL_GET_MEMBER + "/" + param._id,
        param
      );

      console.log({ resEditMember });
      return true;
    } else {
      const resGetMember = await axios.get(URL_GET_MEMBER);
      console.log({ resGetMember });
      return resGetMember.data;
    }
  } catch (error) {
    console.log({ error });

    return [];
  }
};
export default getMember;
