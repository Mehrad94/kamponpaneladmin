import React, { Component } from "react";
import "./index.css";

export default class OwnersRow extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    console.log({ mojtaba: this.props.owner });
    return (
      <div
        className="owner_row-container"
        style={{
          boxShadow: !this.props.owner.oIsActive && this.props.boxShadow
        }}
      >
        <img className="owner_avatar" src={this.props.owner.oAvatar} />
        <div className="owner_Basic_information">
          <div className="owner_jus">
            <span className="owner_discription">
              <span className="owner_name">{this.props.owner.oName}</span>
              <span className="owner_category">
                {this.props.owner.oCategory}
              </span>
            </span>
          </div>

          <div className="phone_address-owner">
            <span className="owner_address">{this.props.owner.oAddress}</span>
            <span className="owner_phone-number">
              {this.props.owner.oPhoneNumber}
            </span>
          </div>
        </div>

        <div className="owner_job">
          <div className="kapons_owner">
            <span> کل کمپن های : </span>

            <span>{this.props.owner.kampons}</span>
          </div>
          <div className="kapons_owner">
            <span> کمپن های استفاده نشده : </span>

            <span>{this.props.owner.notUsedOrders}</span>
          </div>
          <div className="sell_owner">
            <span> فروخته شده : </span>

            <span>{this.props.owner.usedOrders}</span>
          </div>
          <div className="sell_balence">
            <span>موجودی حساب :</span>
            <span>{this.props.owner.balance}</span>
          </div>
        </div>
        <div className="owner_active">
          <div className="owner_activing">
            <span className="owner_is-active">فعال:</span>
            <span className="owner_active_discount">
              {this.props.owner.activeKampons}
            </span>
          </div>
          <div className="owner_de-active">
            <span className="owner_is-de-active">غیرفعال:</span>
            <span className="owner_de-active_discount">
              {this.props.owner.deactiveKampons}
            </span>
          </div>
        </div>
        <div className="panel_options">
          <button className="delete" onClick={this.props.onDelete}>
            x
          </button>
          <button className="edit" onClick={this.props.edit} />
          <button className="block_owner" onClick={this.props.ownerBlock}>
            --
          </button>
        </div>
      </div>
    );
  }
}
