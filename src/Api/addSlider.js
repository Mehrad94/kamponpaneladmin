import axios from "axios";
import { URL_ADD_SLIDER } from "../values/strings";

const addSlider = async params => {
  console.log({ params });
  try {
    const url = URL_ADD_SLIDER;
    console.log({ url: url });
    const resgetSlider = await axios.post(url, params);
    console.log({ resgetSlider });
    return true;
  } catch (error) {
    console.log({ error });
    return false;
  }
};
export default addSlider;
