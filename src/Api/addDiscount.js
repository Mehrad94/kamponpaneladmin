import axios from "axios";
import { URL_ADD_DISCOUNT } from "../values/strings";

const addDiscount = async (objParams, edit) => {
  console.log("add discount");
  console.log({ objParams });
  try {
    if (edit == true) {
      const resEditDiscount = await axios.put(
        URL_ADD_DISCOUNT + "/" + objParams._id,
        objParams
      );
      console.log({ resEditDiscount });
    } else {
      const resAddDiscount = await axios.post(URL_ADD_DISCOUNT, objParams);
      console.log({ resAddDiscount });
    }

    return true;
  } catch (error) {
    console.log({ error });
    return false;
  }
};
export default addDiscount;
