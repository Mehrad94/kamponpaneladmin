import React, { Component } from "react";
import "./index.scss";
export default class DiscountRow extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    // console.log({ moj: this.props });
    const {
      resSearch,
      discount,
      boxShadow,
      edit,
      onDelete,
      block
    } = this.props;
    // let dis = [];
    // if (resSearch === []) {
    //   // discount.map(discount => discount.push({ discount }));
    //   dis.push({ discount });
    // } else {
    //   // resSearch.map(search => dis.push({ search }));
    //   dis.push({ resSearch });
    // }

    // console.log({ discount, resSearch });
    return (
      <div
        id="discounts"
        style={{
          boxShadow: !discount.isActive && boxShadow
        }}
      >
        <div className="discount_img">
          <img className="discount_img-view" src={discount.disImages[0]} />
          <span className="percent"> %{discount.disPercent}</span>
        </div>

        <div className="discount_titles">
          <div className="titles">
            <span className="cat_title">{discount.disOwner}</span>
            <span className="cat_title">{discount.disTitle}</span>
            <span className="cat_title font12">{discount.disDescription}</span>
          </div>

          <div className="price_discount">
            <div className="sell">
              <span>فروخته شده:</span>
              <span>{discount.boughtCount}</span>
            </div>
            <del style={{ color: "red" }} className="discount_RealPrice">
              {discount.disRealPrice}
            </del>
            <span style={{ color: "green" }} className="discount_NewPrice">
              {discount.disNewPrice}
              <span className="toman">تومان</span>
            </span>
          </div>
          <div className="panel_options">
            <button className="delete" onClick={onDelete}>
              x
            </button>
            <button className="edit" onClick={edit} />
            <button className="block_owner" onClick={block}>
              --
            </button>
          </div>
        </div>
      </div>
    );
  }
}
// disTitle: "",
// disDescription: "",
// disImages: [],
// disRealPrice: "",
// disNewPrice: "",
// disCategory: " ",
// disOwner: "",
// disFdiscounteateres: [],
// disTermsOfUse: []
