import React, { Component } from "react";
import "./index.scss";
import { TOKEN } from "../../values/strings/index";
import { withdrawals, cashOut } from "../../Api";
export default class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Transactions: [],
      isWaiting: true
    };
  }
  async componentDidMount() {
    await this._handelWithdrawal("Waiting", true, "tabOne");
  }

  _handelWithdrawal = async (into, isWaiting, tabs) => {
    this.setState({ tabs });
    const withdrawal = await withdrawals(into);
    console.log({ withdrawal });

    this.setState({ Transactions: withdrawal, isWaiting });
  };

  render() {
    const { Transactions } = this.state;
    console.log({ Transactions });

    return (
      <div className="navbar">
        <div className="navbar_container">
          <div className="navbar_box">
            <div className="navbar_box-right">
              <div className="Nav_box-right">
                <div className="admin_information">
                  <div className="title_name">
                    <a>mehrad</a>
                  </div>
                  <div className="admin-image">
                    <img src={require("../../assets/images/index.jpg")} />
                  </div>
                </div>
              </div>
              <div className="Nav_box-left">
                <div className="left_nav-box">
                  <div className="menu-icon">
                    <i className="icon_information" />
                    <span>{Transactions.length}</span>
                  </div>
                  <div className="menu-icon">
                    <i className="icon_information" />
                    <span>{Transactions.length}</span>
                  </div>
                  <div className="menu-icon">
                    <i className="icon_information" />
                    <span>{Transactions.length}</span>
                  </div>
                  <div className="menu-icon">
                    <i className="icon_information" />
                    <span>{Transactions.length}</span>
                  </div>
                </div>
              </div>
            </div>
            <div className="navbar_box-left">
              <a className="navbar_box-left-logo">
                <div />
                <span>کمپن</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
