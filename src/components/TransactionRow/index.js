import React from "react";
import "./index.scss";
const TransactionRow = ({ Transaction, reply, isWaiting }) => {
  console.log({ mojiTransaction: Transaction });
  return (
    <div id="Transactions">
      <div className="Transaction_img">
        <img className="Transaction_img-view" src={Transaction.owner.oAvatar} />
      </div>
      <div className="Transaction_titles">
        <span className="Transaction_title">{Transaction.owner.oName}</span>
        <span className="Transaction_title price">{Transaction.amount}</span>
      </div>
      <div className="Transaction_date">
        <div className="Transaction_date-true">
          <span>زمان</span>
          <span>{Transaction.time} </span>
        </div>
      </div>

      {isWaiting && (
        <React.Fragment>
          <button
            className="accept_Transaction"
            onClick={() => reply("Accepted", Transaction._id)}
          >
            تایید
          </button>
          <button
            className="delete_Transaction"
            onClick={() => reply("Rejected", Transaction._id)}
          >
            رد
          </button>
        </React.Fragment>
      )}
    </div>
  );
};

export default TransactionRow;
//esm zaman mablagh axowner
//accept reject
