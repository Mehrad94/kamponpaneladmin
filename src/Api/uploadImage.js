import axios from "axios";

import { URL_UPLOAD_IMAGE, BASE_URL } from "./../values/strings";

const uploadImage = async imgFile => {
  try {
    console.log({ imgFile });
    var formData = new FormData();

    formData.append("image", imgFile);
    console.log("Sending ...");
    console.log({ URL_UPLOAD_IMAGE });
    let resultUploadImage = await axios.post(URL_UPLOAD_IMAGE, formData, {
      onDownloadProgress: progressEvent => {
        let percentCompleted = Math.round(
          (progressEvent.loaded * 100) / progressEvent.total
        );
        console.log(progressEvent.lengthComputable);
        console.log(percentCompleted);
      }
    });
    console.log({ resultUploadImage });
    return resultUploadImage.data.url;
  } catch (error) {
    console.log({ error });
    return [];
  }
};
export default uploadImage;
