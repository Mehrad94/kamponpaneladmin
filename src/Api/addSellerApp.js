import axios from "axios";
import { URL_ADD_APP_UPLOAD } from "../values/strings";

const addSellerApp = async params => {
  console.log({ params });

  try {
    const resAddSellerApp = await axios.post(URL_ADD_APP_UPLOAD, params);
    console.log({ url: resAddSellerApp });
    return true;
  } catch (error) {
    console.log({ error });
    return false;
  }
};
export default addSellerApp;
