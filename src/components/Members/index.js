import React, { Component } from "react";
import "./index.scss";
// import { Link } from "react-router-dom";
import { getMember } from "../../Api";
import MembersRow from "../MemberRow";
import { TOKEN } from "../../values/strings";
import { aSuccess, aFailed } from "../../utils";
export default class Members extends Component {
  constructor(props) {
    super(props);
    this.state = {
      members: [],
      edit: true,
      boxShadow: "1px 1px 8px 1px red"
    };
  }
  async componentDidMount() {
    const resGetMember = await getMember();
    console.log({ resGetMember });
    this.setState({ members: resGetMember });
  }

  _handelMemberBlock = async (_id, mIsActive) => {
    mIsActive = !mIsActive;
    const { edit } = this.state;
    const param = { _id, mIsActive };
    console.log({ param });

    console.log({ _id });
    if (await getMember(param, edit)) {
      aSuccess("تایید", "با موفقیت انجام شد");
      {
        mIsActive
          ? this.setState({ boxShadow: "1px 3px 8px 1px rgba(0, 0, 0, 0.2)" })
          : this.setState({ boxShadow: "1px 1px 8px 1px red" });
      }
    } else aFailed("خطا", "شما موف به مسدود کردن این ممبر نشدید");
  };

  render() {
    console.log({ state: this.state });

    return (
      <div className="member_container">
        <div className="member_head">
          <div className="member_title"> ممبر ها</div>
        </div>
        <div className="member_list">
          {this.state.members.map(member => (
            <MembersRow
              member={member}
              memberBlock={() =>
                this._handelMemberBlock(member._id, member.mIsActive)
              }
              boxShadow={this.state.boxShadow}
              onDelete={() => {
                this._handelDelete(member._id);
              }}
            />
          ))}
        </div>
      </div>
    );
  }
}
