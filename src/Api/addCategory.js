import axios from "axios";
import { URL_CATEGORY } from "./../values/strings";

const addCategory = async (params, edit, catId, subCategory) => {
  console.log({ params, subCategory });
  const { catImage, catTitle, catPersianTitle, _id, weight } = params;
  console.log({ params, edit, catId });
  const { categoryId, subCatTitle, subCatTitlePersian } = subCategory;
  console.log({ categoryId, subCatTitlePersian, subCatTitle });

  try {
    if (subCategory) {
      let params = { subCatTitle, subCatTitlePersian };
      await axios.post(URL_CATEGORY + "/subCategory/" + categoryId, params);
      return true;
    } else if (edit) {
      const resEditCategory = await axios.put(URL_CATEGORY + "/" + params._id, {
        catTitle,
        catImage,
        catPersianTitle
      });
      console.log({ resEditCategory });

      return true;
    } else {
      await axios.post(URL_CATEGORY, { catTitle, catImage, catPersianTitle });
      return true;
    }
  } catch (error) {
    console.log({ error });
    return false;
  }
};
export default addCategory;
