import React, { Component } from "react";
import "./index.scss";
import { aSuccess, aFailed } from "../../utils";
import Swal from "sweetalert2";
import "sweetalert2/src/sweetalert2.scss";
import { uploadImage, addCategory, getCategories } from "../../Api";
import { TOKEN } from "../../values/strings";

const defaultAvatar =
  "https://cdn2.iconfinder.com/data/icons/instagram-ui/48/jee-75-512.png";
export default class AddCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      catTitle: "",
      catPersianTitle: "",
      imageFile: null,
      isImageUpload: false,
      catImage: defaultAvatar,
      edit: false
    };
  }
  locationcategories = window.location.href;

  categoriesLocationId = this.locationcategories.split("/");

  locationLength = this.categoriesLocationId.length;

  async componentDidMount() {
    let categoriesId = this.categoriesLocationId[
      this.categoriesLocationId.length - 1
    ];
    console.log({ categoriesId });

    if (this.locationLength == 5) {
      let allcategories = await getCategories();
      const categories = allcategories.filter(categories => {
        return categories._id === categoriesId;
      });
      this.setState(categories[0]);
      this.setState({ edit: true });
      console.log({ categories });
      console.log({ allcategories });
    }
  }
  _handelSubmit = async e => {
    e.preventDefault();
    const {
      catTitle,
      catImage,
      imageFile,
      isImageUpload,
      _id,
      edit,
      catPersianTitle
    } = this.state;
    if (catTitle == "" || catPersianTitle == "" || !isImageUpload)
      return aFailed("خطا", "لطفا اطلاعات درخواستی را کامل کنید");

    console.log({ catTitle, catImage, catPersianTitle });
    const params = { catTitle, catImage, catPersianTitle, _id };
    if (await addCategory(params, edit, "", "")) {
      this.setState({
        catTitle: "",
        catImage: defaultAvatar,
        catPersianTitle: ""
      });
      aSuccess("ثبت شد");
    } else aFailed("خطا", "لطفا اطلاعات وارد شده خود را بررسی نمایید");
  };

  _handleChange = e => {
    let value = e.currentTarget.value;
    let newState = this.state;
    newState[e.currentTarget.name] = value;
    this.setState({ state: newState });
  };

  _handelImageUpload = async e => {
    let file = e.currentTarget.files[0];
    await this.setState({ imageFile: file });

    let imgUrl = await uploadImage(this.state.imageFile);
    console.log({ imgUrl });
    this.setState({ catImage: imgUrl, isImageUpload: true });
  };
  render() {
    const locationLength = this.locationLength;
    return (
      <div className="addcategory_container">
        <div className="addcategory_row">
          <div className="addcategory_header-box">
            {locationLength == 5 ? "تغییر دسته" : "مشخصات دسته جدید"}
          </div>
          <form onSubmit={this._handelSubmit}>
            <div className="category_avatar-insert">
              <img
                for="fileupload"
                className="preview"
                src={this.state.catImage}
              />
              <label className="img_browser">
                <span className="add_img-category">+</span>
                <input
                  id="fileupload"
                  name="fileupload"
                  className="category_img-enter "
                  type="file"
                  onChange={this._handelImageUpload}
                />
              </label>
            </div>
            <div className="enter_category-name">
              <span>
                {locationLength == 5
                  ? "  نام جدید دسته را انگلیسی وارد کنید"
                  : "نام دسته جدید را انگلیسی وارد کنید"}
              </span>
              <input
                name="catTitle"
                onChange={this._handleChange}
                value={this.state.catTitle}
              />
            </div>

            <div className="enter_category-name">
              <span>
                {locationLength == 5
                  ? "  نام جدید دسته را فارسی وارد کنید"
                  : "نام دسته جدید را فارسی وارد کنید"}
              </span>
              <input
                name="catPersianTitle"
                onChange={this._handleChange}
                value={this.state.catPersianTitle}
              />
            </div>
            <input
              className="btn_category-submit"
              type="submit"
              value={locationLength == 5 ? "   تغییر    " : "ثبت "}
            />
          </form>
        </div>
      </div>
    );
  }
}
