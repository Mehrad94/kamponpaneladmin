import React, { Component } from "react";
import "./index.scss";
import { Link } from "react-router-dom";
import SliderRow from "../SliderRow";
import { getSlider, getCategories, deleteSlider } from "../../Api";

export default class Sliders extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sliders: [],
      allSliders: [],
      allCategories: [],
      allSubCategory: []
    };
  }
  async componentDidMount() {
    console.log("kabavkjhbadkvjhbadv");

    let allCategories = await getCategories();
    if (allCategories) {
      allCategories.unshift({ catTitle: " main" });
      console.log({ allCategories });
      this.setState({ allCategories });
      const allSliders = await getSlider("main");
      console.log({ allSliders });
      if (allSliders) {
        this.setState({ allSliders, sliders: allSliders });
      }
    }
  }
  // _onClickCategories = async cat => {
  //   const resSlider = await getSlider(cat);
  //   console.log({ resSlider });

  //   this.setState({ sliders: resSlider });
  // };
  _onClickCategories = async cat => {
    console.log({ cat });
    let allSliders = this.state.allSliders;
    let allCategories = this.state.allCategories;

    if (cat === " main") {
      this.setState({ sliders: allSliders });
      return;
    }
    let category = allCategories.filter(category => {
      return category.catTitle === cat;
    });
    if (category.length) {
      let catId = category[0]._id;
      console.log({ category, catId });
      const resGetSubCategory = await getCategories(catId);
      if (resGetSubCategory.data.length) {
        this.setState({ allSubCategory: resGetSubCategory.data });
      } else {
        this.setState({ allSubCategory: "" });
      }
    }

    // console.log({ allSlider });

    // this.setState({ sliders });

    const resSlider = await getSlider(cat);
    console.log({ resSlider });
    if (resSlider.length) {
      this.setState({ sliders: resSlider });
    }
  };
  // _handelDelete = async slider => {
  //   await deleteSlider(slider);
  //   this.componentDidMount();
  // };
  _handelDelete = async sliderId => {
    if (await deleteSlider(sliderId)) {
      console.log("true");
      const allslider = this.state.sliders;

      const sliders = allslider.filter(slider => {
        return slider._id !== sliderId;
      });
      console.log({ sliders });

      this.setState({ sliders });
    }
  };
  _handelEdite = (sliderId, sliderStype) => {
    console.log({ abas: sliderId });
    window.location = "/addslider/" + sliderId + "/" + sliderStype;
  };

  _question = value => {
    this.setState({ valueRemove: value });
    this.setState({ modal: "actived " });
  };
  _questionFalse = () => {
    this.setState({ modal: "" });
  };
  _questiontrue = () => {
    let state = this.state;
    let value = state.valueRemove;

    this._handelDelete(value);
    this.setState({ removing: "", modal: "" });
  };
  _handelSubCategory = async e => {
    let value = e.currentTarget.value;
    let allSubCategory = this.state.allSubCategory;
    let subCategoryClicked = allSubCategory.filter(sub => {
      return sub.subCatTitlePersian === value;
    });
    let subId = subCategoryClicked[0]._id;
    const resSlider = await getSlider(subId);
    console.log({ resSlider });
    if (resSlider.length) {
      this.setState({ sliders: resSlider });
    } else {
      this.setState({ sliders: "" });
    }
    console.log({ value, subCategoryClicked, subId });
  };
  render() {
    console.log({ state: this.state });
    const { allSubCategory, sliders } = this.state;
    let key1 = 0;
    let key2 = 0;
    return (
      <div className="slider">
        <div id="modal_question" className={this.state.modal}>
          <span>آیا مطمئن به پاک کردن این مورد هستید؟</span>
          <div className="remove_accept">
            <span className="green" onClick={this._questiontrue}>
              بله
            </span>
            <span className="orange" onClick={this._questionFalse}>
              خیر
            </span>
          </div>
        </div>
        <div className="slider_container">
          <div className="head_slider">
            <div className="slider_title"> اسلاید ها</div>
            <Link to="/addslider">
              <button className="slider_submit">+ثبت</button>
            </Link>
          </div>
          <div className="optionsBox">
            <div className="tab_category">
              <select
                onChange={e => this._onClickCategories(e.currentTarget.value)}
              >
                {this.state.allCategories.map(category => (
                  <option key={key1++}>{category.catTitle}</option>
                ))}
              </select>
            </div>
            {allSubCategory.length ? (
              <div className="tabSubCategory tab_category">
                <select onChange={this._handelSubCategory}>
                  <option>انتخاب کنید</option>
                  {allSubCategory.map(subCategory => (
                    <option key={subCategory._id}>
                      {subCategory.subCatTitlePersian}
                    </option>
                  ))}
                </select>
              </div>
            ) : (
              ""
            )}
          </div>
          <div className="slider_list">
            {sliders &&
              sliders.map(slider => (
                <SliderRow
                  edit={() => this._handelEdite(slider._id, slider.sType)}
                  key={key2++}
                  slider={slider}
                  onDelete={() => {
                    this._question(slider._id);
                  }}
                />
              ))}
          </div>
        </div>
      </div>
    );
  }
}
