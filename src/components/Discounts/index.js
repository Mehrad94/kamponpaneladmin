import React, { Component } from "react";
import "./index.scss";
import { Link } from "react-router-dom";
import {
  getDiscount,
  deleteDiscount,
  getCategories,
  addDiscount,
  Search
} from "../../Api";
import DiscountRow from "../DiscountRow";
import { aSuccess, aFailed } from "../../utils";
export default class Discounts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      discounts: [],
      discount: [],
      allCategories: [],
      valueRemove: "",
      nameRemove: "",
      modal: "",
      removing: "",
      boxShadow: "1px 1px 8px 1px red",
      edit: true,
      cat: "همه",
      numberPage: [],
      loading: true,
      page: 1,
      resSearch: []
    };
  }
  async componentDidMount() {
    this._handelGetDiscount();
  }
  _handelDelete = async DiscountId => {
    if (await deleteDiscount(DiscountId)) {
      console.log("true");
      const alldiscount = this.state.discount;

      const discount = alldiscount.filter(discount => {
        return discount._id !== DiscountId;
      });
      console.log({ discount });

      this.setState({ discount });
    }
  };

  _question = value => {
    this.setState({ valueRemove: value });
    this.setState({ modal: "actived " });
  };
  _questionFalse = () => {
    this.setState({ modal: "" });
  };
  _questiontrue = () => {
    let state = this.state;
    let value = state.valueRemove;

    this._handelDelete(value);
    this.setState({ removing: "", modal: "" });
  };
  _handelEdite = discountId => {
    console.log({ abas: discountId });
    window.location = "/addDiscount/" + discountId;
  };
  _handelGetDiscount = async (cat, pages) => {
    console.log({ catii: cat });
    const page = this.state.page;
    let Npage = "";
    if (!pages) {
      Npage = page;
    } else {
      Npage = pages;
    }

    if (cat !== "همه") {
      const resGetDiscount = await getDiscount(cat, Npage);

      if (resGetDiscount.lenght <= 0 && resGetDiscount.docs.length === 0) {
        aFailed("این دسته خالی میباشد");
      } else {
        console.log("omadToosh");
        if (resGetDiscount) {
          let discount = resGetDiscount.docs;
          let pages = resGetDiscount.pages;
          const numberPage = [];
          for (let i = 1; i <= pages; i++) {
            numberPage.push(i);
          }

          console.log({ resGetDiscount, moji: discount });
          this.setState({
            discounts: discount,
            discount,
            loading: false,
            cat,
            numberPage
          });
        }
      }
    } else {
      const resGetDiscount = await getDiscount("", Npage);

      if (resGetDiscount.docs) {
        let discount = resGetDiscount.docs;
        let pages = resGetDiscount.pages;

        const numberPage = [];
        for (let i = 1; i <= pages; i++) {
          console.log({ i });
          numberPage.push(i);
        }
        this.setState({
          discounts: discount,
          discount,
          cat: "همه",
          numberPage
        });
      }
    }
    const allCategories = await getCategories();
    allCategories.unshift({ catTitle: "همه" });
    console.log({ allCategories });
    this.setState({ allCategories });
  };
  _onClickCategories = (cat, page) => {
    !page && this.setState({ page: 1 });
    if (cat === "همه") {
      this._handelGetDiscount("", page);
    } else {
      this._handelGetDiscount(cat, page);
    }
    // console.log({ ok });
    // if (!cat) cat = this.state.cat;
    // console.log({ catOne: cat });
    // let discounts = this.state.discounts;
    // let allCategories = this.state.allCategories;
    // let discount = {};
    // if (ok === "1" && !all) {
    //   console.log("ok ok ok");
    //   discount = discounts.filter(discount => {
    //     return discount.disCategory === cat && discount.isActive === true;
    //   });
    // } else if (ok === "1" && all === true) {
    //   console.log("yeeeees yeees");
    //   discount = discounts.filter(discount => {
    //     return discount && discount.isActive === true;
    //   });
    //   console.log({ discountAsli: discount });
    // } else if (cat === "همه") {
    //   console.log("hame ham omad");
    //   this.setState({ discount: discounts, cat: "همه" });
    //   return;
    // } else if (!ok) {
    //   discount = discounts.filter(discount => {
    //     return discount.disCategory === cat;
    //   });
    // }
    // console.log({ discount });
    // if (cat === "همه") {
    //   this.setState({ discount });
    // } else if (!discount.length == 0) {
    //   this.setState({ cat: discount[0].disCategory, discount });
    // } else if (!search) aFailed("این دسته خالی میباشد");
  };

  _handelFilterOwner = search => {
    let cat = this.state.cat;
    console.log({ filter: cat });
    let ok = "1";
    let all = false;
    if (cat === "همه") {
      all = true;
    }
    this._onClickCategories(cat, ok, all, search);
  };

  _handelFilterOwnerBlock = () => {
    let discounts = this.state.discounts;
    let cat = this.state.cat;
    console.log({ catBlock: cat });
    let discount = [];
    if (cat === "همه") {
      discount = discounts.filter(discount => {
        return discount.isActive === false && discount.disCategory;
      });
    } else {
      discount = discounts.filter(discount => {
        return discount.isActive === false && discount.disCategory === cat;
      });
    }
    console.log({ discount1: discount });
    if (!discount.length == 0) {
      this.setState({ discount });
    } else aFailed(" خالی میباشد");
  };
  _handelSearch = async e => {
    // this.setState({ loading: true });

    let value = e.currentTarget.value;
    if (value.length >= 1) {
      let discount = await Search(value);
      console.log({ discount });
      if (discount) {
        this.setState({ discount: discount.discounts });
      }
    } else {
      const { discounts: discount } = this.state;
      this.setState({ discount });
    }

    console.log({ value });
    // let discount = [];
    // let cat = this.state.cat;
    // let discounts = this.state.discounts;

    // if (value == null) {
    //   let search = "1";
    //   this._handelFilterOwner(search);
    //   return;
    // }
    // discount = discounts.filter(discount => {
    //   if (cat === "همه") {
    //     return (
    //       discount.disDescription.includes(value) ||
    //       discount.disTitle.includes(value)
    //     );
    //   } else {
    //     return (
    //       (discount.disDescription.includes(value) ||
    //         discount.disTitle.includes(value)) &&
    //       discount.disCategory.includes(cat)
    //     );
    //   }
    // });
    // console.log({ value, discount });
    // this.setState({ discount });
  };

  _handelDiscountBlock = async (_id, isActive) => {
    isActive = !isActive;
    const { edit } = this.state;
    const param = { _id, isActive };
    console.log({ param });

    console.log({ _id });
    if (await addDiscount(param, edit)) {
      aSuccess("تایید", "با موفقیت انجام شد");
      {
        isActive
          ? this.setState({ boxShadow: "1px 3px 8px 1px rgba(0, 0, 0, 0.2)" })
          : this.setState({ boxShadow: "1px 1px 8px 1px red" });
      }
    } else aFailed("خطا", "شما موفق به مسدود کردن این فروشنده نشدید");
  };
  _handelchengePage = page => {
    const { cat } = this.state;
    this.setState({ page });
    this._handelGetDiscount(cat, page);
  };
  render() {
    console.log({ ll: this.state.cat });
    let key = 0;
    console.log({ STATE: this.state });
    const {
      loading,
      discount,
      pages,
      numberPage,
      page,
      resSearch
    } = this.state;
    return (
      <div className="discount_container">
        <div className="discount_head">
          <div className="discount_title"> تخفیف ها</div>
          <Link to="/addDiscount">
            <button className="discount_submit">+ ثبت</button>
          </Link>
        </div>
        <div className="owner_option">
          <div className="all_category">
            <select
              value={this.state.cat}
              onChange={e => this._onClickCategories(e.currentTarget.value)}
            >
              {loading === false &&
                this.state.allCategories.map(category => (
                  <option key={category._id}> {category.catTitle}</option>
                ))}
            </select>
          </div>
          <div className="owner-block block">
            <span onClick={this._handelFilterOwnerBlock}>
              مشاهده غیر فعال شدها
            </span>
          </div>
          <div className="owner-block unBlock">
            <span onClick={this._handelFilterOwner}>مشاهده فعال ها</span>
          </div>
          <div className="owner-search">
            <input onChange={this._handelSearch} placeholder="جستجو...." />
          </div>
        </div>
        <div id="modal_question" className={this.state.modal}>
          <span>آیا مطمئن به پاک کردن این مورد هستید؟</span>
          <div className="remove_accept">
            <span className="green" onClick={this._questiontrue}>
              بله
            </span>
            <span className="orange" onClick={this._questionFalse}>
              خیر
            </span>
          </div>
        </div>
        <div className="discount_list">
          {loading === false
            ? discount.map(discount => (
                <DiscountRow
                  key={key++}
                  boxShadow={this.state.boxShadow}
                  edit={() => this._handelEdite(discount._id)}
                  discount={discount}
                  resSearch={resSearch}
                  onDelete={() => {
                    this._question(discount._id);
                  }}
                  block={() =>
                    this._handelDiscountBlock(discount._id, discount.isActive)
                  }
                />
              ))
            : null}
        </div>
        <div className="pages">
          {!loading &&
            numberPage.map(page => (
              <span
                onClick={() => this._handelchengePage(page)}
                className={this.state.page === page ? "page actived" : "page"}
              >
                {page}
              </span>
            ))}
        </div>
      </div>
    );
  }
}
