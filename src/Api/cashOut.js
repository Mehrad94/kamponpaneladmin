import axios from "axios";
import { URL_CASHOUT } from "../values/strings";

const cashOut = async param => {
  console.log({ param });
  try {
    const url = URL_CASHOUT;
    console.log({ url: url });
    const resgetSlider = await axios.post(url, param);
    console.log({ resgetSlider });
    return true;
  } catch (error) {
    console.log({ error });
    return false;
  }
};
export default cashOut;
