import axios from "axios";
import { URL_GET_DISCOUNT, URL_GIVE_DISCOUNT } from "../values/strings";

const getDiscount = async (category, pageNumber, id) => {
  console.log({ category, pageNumber, id });
  try {
    if (id) {
      console.log("omadinto");
      const resGetDiscount = await axios.get(URL_GIVE_DISCOUNT + "/" + id);
      console.log(resGetDiscount);
      console.log(resGetDiscount);

      return resGetDiscount.data;
    } else if (!category) {
      console.log("omadinto2");
      const resGetDiscount = await axios.get(
        URL_GET_DISCOUNT + "/" + pageNumber
      );
      console.log(resGetDiscount);
      return resGetDiscount.data;
    } else {
      console.log("omadinto3");
      const resGetDiscount = await axios.get(
        URL_GET_DISCOUNT + "/" + category + "/" + pageNumber
      );

      return resGetDiscount.data;
    }
  } catch (error) {
    console.log({ error });

    return [];
  }
};
export default getDiscount;
