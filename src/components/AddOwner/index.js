import React, { Component } from "react";
import {
  uploadImage,
  addOwner,
  getCategories,
  getDistrict,
  getOwners
} from "../../Api";
import "./index.css";
import { aSuccess, aFailed } from "../../utils";

const defaultAvatar =
  "https://cdn2.iconfinder.com/data/icons/instagram-ui/48/jee-75-512.png";
export default class AddOwner extends Component {
  state = {
    oName: "",
    oAddress: "",
    oPhoneNumber: "",
    oDistrict: "",
    oCategory: "",
    oOtherNumbers: [],
    otherPhone: "",
    isImageUpload: false,
    oAvatar: defaultAvatar,
    imageFile: null,
    allCategories: [],
    allDistricts: [],
    valueRemove: "",
    nameRemove: "",
    modal: "",
    removing: "",
    _id: "",
    edit: ""
  };

  locationOwner = window.location.href;

  ownerLocationId = this.locationOwner.split("/");

  locationLength = this.ownerLocationId.length;

  async componentDidMount() {
    console.log({ javad: this.props.location });

    let allCategories = await getCategories();
    console.log({ allCategories });

    this.setState({ allCategories, oCategory: allCategories[0].catTitle });
    console.log("okl0-okl0-okl");
    const allDistricts = await getDistrict();
    console.log({ allDistricts });
    this.setState({ allDistricts, oDistrict: allDistricts[0].districtName });

    let ownerId = this.ownerLocationId[this.ownerLocationId.length - 1];
    console.log({ ownerId });

    if (this.locationLength == 5) {
      let allOwners = await getOwners();
      const owner = allOwners.filter(owners => {
        return owners._id === ownerId;
      });
      this.setState(owner[0]);
      this.setState({ isImageUpload: true, edit: true });
      console.log({ owner });
      console.log({ allOwners });
    }
    console.log(this.ownerLocationId);
  }

  _handleSubmit = async e => {
    e.preventDefault();
    console.log("submited");

    const {
      oName,
      oAvatar,
      oAddress,
      oPhoneNumber,
      oDistrict,
      oCategory,
      isImageUpload,
      oOtherNumbers,
      _id,
      edit
    } = this.state;

    let params = {
      oName,
      oAvatar,
      oAddress,
      oPhoneNumber,
      oDistrict,
      oOtherNumbers,
      oCategory,
      _id
    };
    if (
      oName == "" ||
      oAvatar == "" ||
      oAddress == "" ||
      oPhoneNumber == "" ||
      oDistrict == "" ||
      oCategory == "" ||
      !isImageUpload
    )
      return "";
    console.log({ state: this.state });
    if (await addOwner(params, edit)) {
      this.setState({
        oName: "",
        oAvatar: "",
        oAddress: "",
        oPhoneNumber: "",
        oAvatar: "",
        oDistrict: "",
        oAvatar: "",
        oCategory: "",
        oAvatar: "",
        oOtherNumbers: [],
        otherMobile: "",
        oAvatar: defaultAvatar,
        _id: ""
      });
      aSuccess("تایید", "با موفقیت انجام شد");
    } else aFailed("خطا", "لطفا همه قسمت های درخواستی را پر نمایید");
  };

  _handleChange = e => {
    let value = e.currentTarget.value;
    let newState = this.state;
    newState[e.currentTarget.name] = value;
    console.log({ newState });
    this.setState({ state: newState });
  };
  _handelImageUpload = async e => {
    let file = e.currentTarget.files[0];
    await this.setState({ imageFile: file });

    let imgUrl = await uploadImage(this.state.imageFile);
    console.log({ imgUrl });
    this.setState({ oAvatar: imgUrl, isImageUpload: true });
  };
  _onClickCategories = async cat => {
    console.log({ cat });
    console.log("cats");
    this.setState({ oCategory: cat });
  };
  _onClickDistrictes = async district => {
    console.log({ district });
    console.log("districts");
    this.setState({ oDistrict: district });
  };
  _onOtherPhone = e => {
    let otherPhone = this.state.otherPhone;
    console.log({ phone: otherPhone });
    let allState = this.state;
    allState[e.currentTarget.name] = otherPhone;
    let oOtherNumbers = this.state.oOtherNumbers;
    oOtherNumbers.push(otherPhone);
    this.setState({ oOtherNumbers, otherPhone: "" });
    console.log({ d: allState, oOtherNumbers });
  };

  _onDeletePhone = value => {
    console.log("54156464");

    const oOtherNumbers = this.state.oOtherNumbers.filter(otherMobile => {
      return otherMobile !== value;
    });
    console.log(oOtherNumbers);
    this.setState({ oOtherNumbers });
  };
  _question = value => {
    this.setState({ valueRemove: value });
    this.setState({ modal: "actived " });
  };
  _questionFalse = () => {
    this.setState({ modal: "" });
  };
  _questiontrue = () => {
    let state = this.state;
    let value = state.valueRemove;

    this._onDeletePhone(value);
    this.setState({ removing: "", modal: "" });
  };

  render() {
    console.log({ Meeehehhasdsdsdsdsdehe: this.params });

    console.log({ state: this.state });

    const locationLength = this.locationLength;

    return (
      <div className="owner_details">
        <div id="modal_question" className={this.state.modal}>
          <span>آیا مطمئن به پاک کردن این مورد هستید؟</span>
          <div className="remove_accept">
            <span className="green" onClick={this._questiontrue}>
              بله
            </span>
            <span className="orange" onClick={this._questionFalse}>
              خیر
            </span>
          </div>
        </div>
        <div className="owner_head-title">
          {locationLength == 5
            ? "تغییر مشخصات صاحب تخفیف"
            : "مشخصات صاحب تخفیف"}
        </div>
        <form
          className="owner_form"
          enctype="multipart/form-data"
          onSubmit={this._handleSubmit}
        >
          <div className="owner_avatar-insert">
            <img
              for="fileupload"
              className="preview"
              src={this.state.oAvatar}
            />
            <label className="img_browser">
              <span className="add_img-owner">+</span>
              <input
                id="fileupload"
                name="fileupload"
                className="owner_img-enter "
                type="file"
                onChange={this._handelImageUpload}
              />
            </label>
          </div>
          <div className="owner_row">
            <span className="owner_row-title">نام فروشنده را وارد کنید:</span>
            <input
              placeholder="نام را وارد کنید"
              className="owner_row-value"
              name="oName"
              type="text"
              value={this.state.oName}
              onChange={this._handleChange}
            />
          </div>
          <div className="owner_row">
            <span className="owner_row-title">آدرس را وارد کنید:</span>
            <input
              placeholder="آدرس را وارد کنید"
              className="owner_row-value"
              name="oAddress"
              type="text"
              value={this.state.oAddress}
              onChange={this._handleChange}
            />
          </div>
          <div className="owner_row">
            <span className="owner_row-title">موبایل را وارد کنید:</span>
            <input
              placeholder="09xxxxxxxxx"
              className="owner_row-value"
              name="oPhoneNumber"
              type="text"
              value={this.state.oPhoneNumber}
              onChange={this._handleChange}
            />
          </div>
          <div className="owner_row positionReletive">
            <span className="enter_mobile" onClick={this._onOtherPhone}>
              ثبت
            </span>
            <div className="span_other-mobile">
              {this.state.oOtherNumbers.map(otherNumbers => (
                <span onClick={() => this._question(otherNumbers)}>
                  {otherNumbers}
                </span>
              ))}
            </div>
            <span className="owner_row-title">تلفن های دیگر را وارد کنید:</span>
            <input
              placeholder="تلفن را وارد کنید"
              className="owner_row-value"
              name="otherPhone"
              type="text"
              value={this.state.otherPhone}
              onChange={this._handleChange}
            />
          </div>
          <div className="owner_row">
            <span className="owner_row-title">موقعیت شهر را وارد کنید:</span>

            <select
              className="owner_row-value"
              name="oDistrict"
              value={this.state.oDistrict}
              onChange={this._handleChange}
              onChange={e => this._onClickDistrictes(e.currentTarget.value)}
            >
              {this.state.allDistricts.map(district => (
                <option>{district.districtName}</option>
              ))}
            </select>
          </div>
          <div className="owner_row">
            <span className="owner_row-title">دسته را وارد کنید:</span>

            <select
              className="owner_row-value"
              name="oCategory"
              value={this.state.oCategory}
              onChange={e => this._onClickCategories(e.currentTarget.value)}
            >
              {this.state.allCategories.map(category => (
                <option>{category.catTitle}</option>
              ))}
            </select>
          </div>

          {/* <div className="owner_row">
            <span className="owner_row-title">موقعیت مکانی را وارد کنید:</span>
            <input
              className="owner_row-value"
              name=""
              type="text"
              onChange={this._handleChange}

              // value={this.state.oCategory}
            // onChange={this._handleChange}
            />
          </div> */}
          <div className="owner_details-send">
            <input
              type="submit"
              value={locationLength == 5 ? "تغییر و ارسال" : "ثبت و ارسال"}
            />
          </div>

          <h1 />
        </form>
      </div>
    );
  }
}
