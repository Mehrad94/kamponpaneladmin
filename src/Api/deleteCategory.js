import axios from "axios";
import { URL_CATEGORY } from "./../values/strings/index";

const deleteCategory = async catId => {
  try {
    const deleteCat = await axios.delete(URL_CATEGORY + "/" + catId);
    console.log({ deleteCat });
  } catch (error) {
    console.log({ error });
  }
};
export default deleteCategory;
