import axios from "axios";
import { URL_DELETE_SLIDER } from "../values/strings";

const deleteSlider = async sliderId => {
  try {
    const resGetslider = await axios.delete(URL_DELETE_SLIDER + "/" + sliderId);
    return resGetslider.data;
  } catch (error) {
    console.log({ error });

    return [];
  }
};
export default deleteSlider;
