import axios from "axios";
import { URL_GET_APP_LINK } from "../values/strings/index";

const getLinks = async () => {
  try {
    console.log("geting app link");
    const resGetAppLink = await axios.get(URL_GET_APP_LINK);
    console.log({ resGetAppLink });
    return resGetAppLink.data;
  } catch (error) {
    console.log({ error });
    return [];
  }
};
export default getLinks;
