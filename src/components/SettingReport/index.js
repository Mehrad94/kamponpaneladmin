import React, { useState, useEffect } from "react";
import "./index.scss";
import { reportSubjects } from "../../Api";

const SettingReport = () => {
  const [subject, setSubject] = useState([]);
  const [subjectType, setSubjectType] = useState("");
  const [modal, setModal] = useState("");
  const [nameRemove, setNameRemove] = useState("");
  const [valueRemove, setValueRemove] = useState("");
  const [set, setSet] = useState("");
  useEffect(() => {
    getSettingReport();
  }, []);

  const getSettingReport = async () => {
    const subject = [];
    if (reportSubjects("", "get")) {
      const resGetreportSubjects = await reportSubjects("", "get");
      const data = resGetreportSubjects.data;
      data.map(dat => subject.push(dat.subject));
      console.log({ data });
      setSubject(data);
    }
  };
  const _handelSubject = e => {
    let value = e.currentTarget.value;
    let val = value;
    // let name = e.currentTarget.name;
    setSubjectType(value);

    console.log({ subjectType });
  };

  const _handelPushSubject = () => {
    // let name = e.currentTarget.name;
    let sets = 0;
    subject.push(subjectType);
    console.log({ subject });
    // setSet(sets++);
    setSubjectType("");
  };
  const _question = value => {
    setValueRemove(value);
    // this.setState({ nameRemove: name, valueRemove: value });
    setModal("actived ");
  };
  const _questionFalse = () => {
    setModal("");
  };
  const _questiontrue = () => {
    let value = valueRemove;
    let name = nameRemove;

    _onDeleteTitles(value, name);
    setModal("");
  };
  const _onDeleteTitles = async (value, name) => {
    console.log("okeye");
    console.log({ value });
    const resdelReportSubject = await reportSubjects(value, "delete");
    console.log(resdelReportSubject);
    getSettingReport();
    // let subjects = subject.filter(sub => {
    //   return sub !== value;
    // });
    // setSubject(subjects);
  };
  const _handelSend = async () => {
    if (await reportSubjects(subjectType, "post")) {
      setSubjectType("");
      getSettingReport();
    }
    // const resReportSubject = await reportSubjects(subjectType, "post");
  };
  let key = 0;
  console.log(subject);
  return (
    <div className="settingReports">
      <div id="modal_question" className={modal}>
        <span>آیا مطمئن به پاک کردن این مورد هستید؟</span>
        <div className="remove_accept">
          <span className="green" onClick={_questiontrue}>
            بله
          </span>
          <span className="orange" onClick={_questionFalse}>
            خیر
          </span>
        </div>
      </div>
      <div className="settingBox">
        <span className="STitleHead">تنظیمات موارد گزارش</span>
        <div className="SBox">
          <div className="STitleSubject">
            {subject.map(subjects => (
              <span key={key++} onClick={() => _question(subjects._id)}>
                {subjects.subject}
              </span>
            ))}
          </div>
          <span>موارد مورد نظرتان را وارد نمایید : </span>
          <textarea
            value={subjectType}
            onChange={_handelSubject}
            name="SubjectType"
          />
          <button onClick={_handelSend}>تایید</button>
        </div>
        {/* <div className="buttonGreen">
          <span onClick={_handelSend}>ارسال</span>
        </div> */}
      </div>
    </div>
  );
};
export default SettingReport;
