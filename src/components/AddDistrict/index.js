import React, { Component } from "react";
import "./index.scss";
import { aSuccess, aFailed } from "../../utils";

import "sweetalert2/src/sweetalert2.scss";
import { addDistrict } from "../../Api";

export default class AddDistrict extends Component {
  constructor(props) {
    super(props);
    this.state = {
      districtName: ""
    };
  }
  _handelSubmit = async e => {
    e.preventDefault();
    const { districtName } = this.state;
    if (districtName == "") return "";

    console.log({ districtName });
    const params = { districtName };
    if (await addDistrict(params)) {
      this.setState({ districtName: "" });
      aSuccess("TITLE", "DESC");
    } else aFailed("k,k,k,", "ikmk,ki,");
  };

  _handleChange = e => {
    let value = e.currentTarget.value;
    let newState = this.state;
    newState[e.currentTarget.name] = value;
    this.setState({ state: newState });
  };

  render() {
    return (
      <div className="addDistrict_container">
        <div className="addDistrict_row">
          <div className="addDistrict_header-box">نام محدوده جدید</div>
          <form onSubmit={this._handelSubmit}>
            <div className="enter_District-name">
              <span>نام محدوده جدید را وارد کنید</span>
              <input
                name="districtName"
                onChange={this._handleChange}
                value={this.state.districtName}
              />
            </div>

            <input className="btn_District-submit" type="submit" value="ثبت " />
          </form>
        </div>
      </div>
    );
  }
}
