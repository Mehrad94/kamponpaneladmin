import React, { Component } from "react";
import { aSuccess, aFailed } from "../../utils";
import { Search, MemberSearch, addGift } from "../../Api";
import "./index.scss";
class AddGift extends Component {
  constructor(props) {
    super(props);
    this.state = {
      disTitle: "",
      searchDiscounts: [],
      isSearchDiscount: "",
      discountId: "",

      memberTitle: "",
      MemberSearchs: [],
      isMemberSearch: "",
      memberId: "",
      count: ""
    };
  }
  _handleSubmit = async e => {
    e.preventDefault();
  };
  _handleChange = async e => {
    let value = e.currentTarget.value;
    let name = e.currentTarget.name;
    let newState = this.state;
    console.log(value);
    newState[name] = value;
    this.setState(newState);
    /////////discountName/////////
    if (name === "disTitle" && value.length > 2) {
      let resSearchDiscount = await Search(value);
      console.log(resSearchDiscount);

      this.setState({
        searchDiscounts: resSearchDiscount.discounts,
        isSearchDiscount: true
      });
      // newState[e.disSearchDiscount].push(searchDiscount);
    } else if (name === "disTitle" && value.length <= 2) {
      this.setState({
        searchDiscounts: "",
        isSearchDiscount: false
      });
    }

    ////////memberName/////////
    if (name === "memberTitle" && value.length > 2) {
      let resMemberSearch = await MemberSearch(value);
      console.log(resMemberSearch);

      this.setState({
        MemberSearchs: resMemberSearch,
        isMemberSearch: true
      });
    } else if (name === "memberTitle" && value.length <= 2) {
      this.setState({
        MemberSearchs: "",
        isMemberSearch: false
      });
    }
  };
  _onSearchItemClick = (name, nameId, nameTitle) => {
    console.log({ nameTitle, nameId });
    // let discount = this.state.discount;
    // discount.disdiscount = discountName;
    // discount.discountId = discountId;
    // console.log({ disdiscounts });
    if (name === "discounts") {
      this.setState({
        isSearchDiscount: false,
        disTitle: nameTitle,
        discountId: nameId
      });
    } else if (name === "members") {
      this.setState({
        isMemberSearch: false,
        memberTitle: nameTitle,
        memberId: nameId
      });
    }
    console.log({ state: this.state });
  };
  _handleSubmit = async e => {
    e.preventDefault();
    const { discountId, memberId, count } = this.state;
    let param = { discountId, memberId, count };

    if (
      !Object.values(param).every(o => {
        if (typeof o === "number") {
          return o.toString().length > 0;
        } else if (typeof o === "string") {
          return o.length > 0;
        } else {
          return true;
        }
      })
    ) {
      return aFailed("اطلاعات ناقص", "لطفا فرم را کامل نمایید");
    }
    if (await addGift(param)) {
      this.setState({
        discountId: "",
        searchDiscounts: "",
        memberId: "",
        count: "",
        memberTitle: "",
        disTitle: "",
        MemberSearchs: ""
      });
      return aSuccess("تایید", "جایزه موفقیت ثبت شد");
    } else
      return aFailed(
        "خطا در ثبت اطلاعات ",
        "متاسفانه اطلاعات ثبت نشد دوباره تلاش کنید"
      );
  };
  render() {
    const {
      searchDiscounts,
      isSearchDiscount,
      disTitle,
      searchDiscount,
      isMemberSearch,
      MemberSearchs,
      memberTitle
    } = this.state;
    console.log({ state: this.state });
    return (
      <div className="addGift_container">
        <div className="addGift_row">
          <div className="GiftHeadTitle">
            <span>جایزه دادن به مشتری</span>
          </div>
          <form className="sendNewGift" onSubmit={this._handleSubmit}>
            <div className="GiftSendInformations">
              {/* //------discount----/// */}
              <div className="giftOfDiscounts GiftROw">
                <span>انتخاب تخفیف</span>
                <input
                  autoComplete="off"
                  value={disTitle}
                  onChange={this._handleChange}
                  name="disTitle"
                  type="text"
                  placeholder="تخفیف مورد نظرتان را تایپ کنید"
                />
                <div
                  className="discount_search"
                  style={
                    isSearchDiscount ? { display: "" } : { display: "none" }
                  }
                >
                  <div className="discount_search-title">
                    {searchDiscounts.length &&
                      searchDiscounts.map(discount => (
                        <div
                          className="discounts"
                          onClick={() =>
                            this._onSearchItemClick(
                              "discounts",
                              discount._id,
                              discount.disTitle
                            )
                          }
                        >
                          <div className="discountImg">
                            <img src={discount.disImages[0]} />
                          </div>
                          <div className="discountNameCategory">
                            <span className="discountName">
                              {discount.disTitle}
                            </span>
                            <span className="discountCategory">
                              {discount.disCategory}
                            </span>
                          </div>
                        </div>
                      ))}
                  </div>
                </div>
              </div>

              {/* //------member----/// */}
              <div className="giftOfDiscounts GiftROw">
                <span> شماره موبایل یا نام کاربر را وارد نمایید </span>
                <input
                  autoComplete="off"
                  onChange={this._handleChange}
                  name="memberTitle"
                  type="text"
                  placeholder="09xxxxxxxxx"
                  value={memberTitle}
                />
                <div
                  className="Member_search"
                  style={isMemberSearch ? { display: "" } : { display: "none" }}
                >
                  <div className="Member_search-title">
                    {MemberSearchs.length &&
                      MemberSearchs.map(Member => (
                        <div
                          className="Members"
                          onClick={() =>
                            this._onSearchItemClick(
                              "members",
                              Member._id,
                              Member.mName
                            )
                          }
                        >
                          <div className="MemberImg">
                            <img src={Member.mAvatar} />
                          </div>
                          <div className="MemberNameCategory">
                            <span className="MemberName">{Member.mName}</span>
                            <span className="MemberCategory">
                              {Member.mPhoneNumber}
                            </span>
                          </div>
                        </div>
                      ))}
                  </div>
                </div>
              </div>
              <div className="countGift">
                <span> تعداد :</span>

                <input name="count" onChange={this._handleChange} />
              </div>
            </div>
            <input
              onClick={this._handleSubmit}
              className="btn_discount-submit"
              type="submit"
              value="ثبت "
            />
          </form>
        </div>
      </div>
    );
  }
}
export default AddGift;
