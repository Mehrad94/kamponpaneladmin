import React, { Component } from "react";
import "./index.scss";
import OwnersRow from "./../OwnersRow";
import { aSuccess, aFailed } from "../../utils";
import { Link } from "react-router-dom";
import { deleteOwner, getOwners, getCategories, addOwner } from "../../Api";
import { TOKEN } from "../../values/strings";
export default class Owner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allOwners: [],
      valueRemove: "",
      owners: [],
      modal: "",
      removing: "",
      allCategories: [],
      boxShadow: "1px 1px 8px 1px red",
      edit: true,
      cat: "همه"
    };
  }

  async componentDidMount() {
    const allOwners = await getOwners();
    console.log({ allOwners });
    this.setState({ allOwners, owners: allOwners });
    const allCategories = await getCategories();

    allCategories.unshift({ catTitle: "همه" });
    console.log({ allCategories });
    this.setState({ allCategories });

    // const resGetOwners = await axios.get(BASE_URL + "/o");

    // this.setState({ owners: resGetOwners.data.owners });
  }
  _handelDelete = async ownerId => {
    if (await deleteOwner(ownerId)) {
      console.log("true");
      const allowner = this.state.owners;

      const owners = allowner.filter(owner => {
        return owner._id !== ownerId;
      });
      console.log({ owners });

      this.setState({ owners });
    }

    this.componentDidMount();
  };
  _handelEdite = ownerId => {
    console.log({ abas: ownerId });
    window.location = "/addOwner/" + ownerId;
  };

  _question = value => {
    this.setState({ valueRemove: value });
    this.setState({ modal: "actived " });
  };
  _questionFalse = () => {
    this.setState({ modal: "" });
  };
  _questiontrue = () => {
    let state = this.state;
    let value = state.valueRemove;

    this._handelDelete(value);
    this.setState({ removing: "", modal: "" });
  };

  _onClickCategories = async (cat, ok, all) => {
    console.log({ ok });
    if (!cat) cat = this.state.cat;
    console.log({ catOne: cat });

    let allOwners = this.state.allOwners;
    let allCategories = this.state.allCategories;
    let owners = {};
    if (ok === "1" && !all) {
      console.log("ok ok ok");

      owners = allOwners.filter(owners => {
        return owners.oCategory === cat && owners.oIsActive === true;
      });
    } else if (ok === "1" && all === true) {
      console.log("yeeeees yeees");

      owners = allOwners.filter(owners => {
        return owners && owners.oIsActive === true;
      });
      console.log({ ownersAsli: owners });
    } else if (cat === "همه") {
      console.log("hame ham omad");
      this.setState({ owners: allOwners, cat: "همه" });

      return;
    } else if (!ok) {
      owners = allOwners.filter(owners => {
        return owners.oCategory === cat;
      });
    }
    console.log({ owners });

    if (cat === "همه") {
      this.setState({ owners });
    } else if (!owners.length == 0) {
      this.setState({ cat: owners[0].oCategory, owners });
    } else aFailed("این دسته خالی میباشد");
  };

  _handelFilterOwner = () => {
    let cat = this.state.cat;
    console.log({ filter: cat });
    let ok = "1";
    let all = false;
    if (cat === "همه") {
      all = true;
    }
    this._onClickCategories(cat, ok, all);
  };

  _handelFilterOwnerBlock = () => {
    let allOwners = this.state.allOwners;
    let cat = this.state.cat;
    console.log({ catBlock: cat });
    let owners = [];
    if (cat === "همه") {
      owners = allOwners.filter(owners => {
        return owners.oIsActive === false && owners.oCategory;
      });
    } else {
      owners = allOwners.filter(owners => {
        return owners.oIsActive === false && owners.oCategory === cat;
      });
    }
    console.log({ owners1: owners });
    if (!owners.length == 0) {
      this.setState({ owners });
    } else aFailed(" خالی میباشد");
  };

  _handelOwnerBlock = async (_id, oIsActive) => {
    oIsActive = !oIsActive;
    const { edit } = this.state;
    const param = { _id, oIsActive };
    console.log({ param });

    console.log({ _id });
    if (await addOwner(param, edit)) {
      aSuccess("تایید", "با موفقیت انجام شد");
      {
        oIsActive
          ? this.setState({ boxShadow: "1px 3px 8px 1px rgba(0, 0, 0, 0.2)" })
          : this.setState({ boxShadow: "1px 1px 8px 1px red" });
      }
    } else aFailed("خطا", "شما موف به مسدود کردن این فروشنده نشدید");
  };
  _handelSearch = e => {
    let owners = [];
    let cat = this.state.cat;
    let allOwners = this.state.allOwners;
    let value = e.currentTarget.value;
    if (value == null) {
      let search = "1";
      this._handelFilterOwner(search);
      return;
    }
    owners = allOwners.filter(owners => {
      if (cat === "همه") {
        return owners.oName.includes(value) || owners.oAddress.includes(value);
      } else {
        return (
          (owners.oName.includes(value) || owners.oAddress.includes(value)) &&
          owners.oCategoryPersian.includes(cat)
        );
      }
    });
    console.log({ value, owners });
    this.setState({ owners });
  };

  render() {
    console.log({ state: this.state });
    return (
      <div className="owner">
        <div id="modal_question" className={this.state.modal}>
          <span>آیا مطمئن به پاک کردن این مورد هستید؟</span>
          <div className="remove_accept">
            <span className="green" onClick={this._questiontrue}>
              بله
            </span>
            <span className="orange" onClick={this._questionFalse}>
              خیر
            </span>
          </div>
        </div>
        <div className="owner_container">
          <div className="head_owner">
            <div className="owner_title"> نام صاحبان تخفیف</div>
            <Link to="/addOwner">
              <button className="owner_submit">+ثبت</button>
            </Link>
          </div>
          <div className="owner_option">
            <div className="all_category">
              <select
                value={this.state.cat}
                onChange={e => this._onClickCategories(e.currentTarget.value)}
              >
                {this.state.allCategories.map(category => (
                  <option> {category.catTitle}</option>
                ))}
              </select>
            </div>
            <div className="owner-block block">
              <span onClick={this._handelFilterOwnerBlock}>
                مشاهده مسدود شدها
              </span>
            </div>
            <div className="owner-block unBlock">
              <span onClick={this._handelFilterOwner}>مشاهده مسدود نشدها</span>
            </div>
            <div className="owner-search">
              <input onChange={this._handelSearch} placeholder="جستجو...." />
            </div>
          </div>

          <div className="owner_list">
            {this.state.owners.map(owner => (
              <OwnersRow
                edit={() => this._handelEdite(owner._id)}
                boxShadow={this.state.boxShadow}
                owner={owner}
                ownerBlock={() =>
                  this._handelOwnerBlock(owner._id, owner.oIsActive)
                }
                onDelete={() => {
                  this._question(owner._id);
                }}
              />
            ))}
          </div>
        </div>
      </div>

      // <div className="owner_container">
      //   <div className="owner_head">
      //     <div className="oners_title">نام صاحبان تخفیف</div>
      //     <div className="oners_submite">
      //       <Link to="/addOwner">
      //         <button>+ثبت</button>
      //       </Link>
      //     </div>
      //   </div>

      //   <div className="owner_list">
      //     {this.state.owners.map(owner => (
      //       <OwnersRow
      //         owner={owner}
      //         onDelete={() => {
      //           this._handelDelete(owner._id);
      //         }}
      //       />
      //     ))}
      //   </div>
      // </div>
    );
  }
}
