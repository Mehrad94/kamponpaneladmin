import Swal from "sweetalert2";
import "sweetalert2/src/sweetalert2.scss";

export const aSuccess = (title, btnTitle) => {
  Swal.fire(title, btnTitle, "success");
};
export const aFailed = (title, text) => {
  Swal.fire({ type: "error", title, text });
};
