import React from "react";
import Axios from "axios";
import { URL_REPORT } from "../values/strings";
const reportSubjects = (subject, send) => {
  console.log(subject);
  const URL = URL_REPORT;
  try {
    console.log({ subject });
    if (send === "post") {
      const reportSubject = Axios.post(URL, { subject });
      console.log({ reportSubject });
      return true;
    } else if (send === "get") {
      const getreportSubject = Axios.get(URL);
      console.log({ getreportSubject });
      return getreportSubject;
    } else if (send === "delete") {
      const reportSubject = Axios.delete(URL + "/" + subject);
      console.log({ reportSubject });
      return true;
    }
  } catch (e) {
    console.log({ e });

    return false;
  }
};
export default reportSubjects;
